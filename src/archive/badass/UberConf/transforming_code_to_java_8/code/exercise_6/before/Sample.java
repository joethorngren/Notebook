import java.util.*;

class Resource {
  public Resource() { System.out.println("Creating...."); }
  public void op1() { System.out.println("some operation 1"); }
  public void op2() { System.out.println("some operation 1"); }
  public void finalize() { System.out.println("external resource cleaned up"); }
}

public class Sample {
	public static void main(String[] args) {
	  Resource resource = new Resource();
	  resource.op1();
	  resource.op2();
	  //cleanup?
  }

	//Problems with this:
	// Changing finalize to close(); but close may never be called!
	// We might next get to the close method if we throw an exception in op1 or op2
	// Might forget to call close
	// Replace calling close with try/catch/finally
	// Another problem: Verbose
	// Java 7: Automatic Resource Management (ARM)
	// ARM says you never have to put finally, move to try with resources

	//Fixed without lambdas
	
	//Java 7-ARM
	public static void main(String[] args) {
		try(Resource resource = new Resource()) {
   		    resource.op1();
		    resource.op2();
		}
	}

	// Fixes verbosity, but still error prone
	// Never trust a feature with "management" in the name
}
