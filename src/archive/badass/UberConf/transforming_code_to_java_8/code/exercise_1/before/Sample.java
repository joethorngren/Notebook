import java.util.*;
import java.util.concurrent.*;

//Exercise 1
//Java 5 code


public class Sample {
	public static void main(String[] args) throws Exception {
	  ExecutorService executorService = Executors.newFixedThreadPool(10);
	  
	  for(int i = 0; i < 10; i++) {
	    final int index = i;				// Without the word final, compiler threatens to burn our village
								// Real reason we put final is because we're submitting a NEW runnable
	    executorService.submit(new Runnable() {		// Object is being creating in main method
	      public void run() {				// With Java 8, you don't have to put the word final on line 13
	        System.out.println("Running task " + index);	
	      }
	    });
	  }
	  
	  System.out.println("Tasks started...");
	  executorService.shutdown();
  }
}

