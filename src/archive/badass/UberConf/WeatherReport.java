package weather;

import java.util.*;
import java.io.*;
import java.net.*;
import org.json.*;

public class WeatherReport {

  public static void main(String[] args) {
    try {

      List<String> cityNames = readCities();
      
      Collections.sort(cityNames);

      List<WeatherData> weatherData = new ArrayList<WeatherData>();
      List<String> warmestCities = new ArrayList<String>();
      double warmestTemperature = -1000;      
      List<String> coldestCities = new ArrayList<String>();
      double coldestTemperature = 1000;
      
      for(String cityName : cityNames) {
        WeatherData weatherDataForCity = fetchWeatherData(cityName);
        weatherData.add(weatherDataForCity);
        
        if(!weatherDataForCity.isError() && weatherDataForCity.getTemperature() == warmestTemperature) {
          warmestCities.add(weatherDataForCity.getCity());
        }
        if(!weatherDataForCity.isError() && weatherDataForCity.getTemperature() > warmestTemperature) {
          warmestTemperature = weatherDataForCity.getTemperature();
          warmestCities = new ArrayList<String>();
          warmestCities.add(weatherDataForCity.getCity());
        }

        if(!weatherDataForCity.isError() && weatherDataForCity.getTemperature() == coldestTemperature) {
          coldestCities.add(weatherDataForCity.getCity());
        }
        if(!weatherDataForCity.isError() && weatherDataForCity.getTemperature() < coldestTemperature) {
          coldestTemperature = weatherDataForCity.getTemperature();
          coldestCities = new ArrayList<String>();
          coldestCities.add(weatherDataForCity.getCity());
        }
      }
      
      System.out.println("Weather details");
      System.out.printf("%-20s %-12s %s\n", "City", "Temperature", "Condition");
      
      for(WeatherData weatherDataForCity : weatherData) {
        if(!weatherDataForCity.isError())
          System.out.printf("%-20s %-12s %s\n", weatherDataForCity.getCity(), weatherDataForCity.getTemperature(), weatherDataForCity.getCondition());
      }
      
      System.out.println("");
      System.out.println("Warmest Cities");
      for(int i = 0; i < warmestCities.size(); i++) {
        System.out.print(warmestCities.get(i));
        if(i != warmestCities.size() - 1)
          System.out.print(", ");
      }

      System.out.println("");
      System.out.println("Coldest Cities");
      for(int i = 0; i < coldestCities.size(); i++) {
        System.out.print(coldestCities.get(i));
        if(i != coldestCities.size() - 1)
          System.out.print(", ");
      }

      System.out.println("");
      System.out.println("Cities that Failed");
      for(WeatherData weatherDataForCity : weatherData) {
        if(weatherDataForCity.isError())
          System.out.printf("%-20s %s\n", weatherDataForCity.getCity(), weatherDataForCity.getError().getMessage());
      }
      
    } catch(Exception ex) {
      System.out.println(ex);
    }
  }
  
  public static List<String> readCities() throws Exception {
    List<String> cityNames = new ArrayList<String>();
    
    BufferedReader reader = new BufferedReader(new FileReader("cities.txt"));
    String line = null;
    while((line = reader.readLine()) != null) {
      if(!line.startsWith("#"))
        cityNames.add(line);
    }
    return cityNames;
  }
  
  public static WeatherData fetchWeatherData(String cityName) {
    try {
      URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q=" + cityName + ",us&units=imperial");
      Scanner scanner = new Scanner(url.openStream());
      String jsonData = scanner.nextLine();
      
      JSONObject weatherDetails = new JSONObject(jsonData);
      String condition = weatherDetails.getJSONArray("weather").getJSONObject(0).getString("description");
      
      return new WeatherData(weatherDetails.getString("name"), weatherDetails.getJSONObject("main").getDouble("temp"), condition);      
    } catch(Exception ex) {
      return new WeatherData(cityName, ex);
    }
  }
}

1. Get list of cities
2. Get weather data for those cities
3. Parse weather data from JSON to WeatherDataBeans 
4. Print Data
     -For each city
     -Warmest Cities
     -Hottest cities
     -Error messages

Decouple File I/O and Data Fetching from Main

