package weather;

public class WeatherData {
  private String city;
  private double temperature;
  private String condition;
  private Exception error;
  
  public WeatherData(String theCity, double theTemperature, String theCondition) {
    city = theCity;
    temperature = theTemperature;
    condition = theCondition;
  }
  
  public WeatherData(String theCity, Exception theException) {
    city = theCity;
    error = theException;
  }
  
  public boolean isError() { return error != null; }
  public String getCity() { return city; }
  public double getTemperature() { return temperature; }
  public String getCondition() { return condition; }
  public Exception getError() { return error; }
}
