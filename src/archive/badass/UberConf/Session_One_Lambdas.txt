The Pathway to Functional Style 

The Evolution of Java: 
Previous upgrades didn't change much 

Lambda Expressions: 
AIC == Missed Opportunities 
Lambdas remove the "ceremony" 

What is a lambda? Anonymous function 
Name, body, parameter list, return type 

Most important part is the body! 
Lambda strips out unnecessary parts giving you just what you want 

Java cares about backward compatibiity 

Languages that don't care about backwards compatibility are doomed 
Languages that Do care about backwards compatibility are doomed 

Java wants to give you new features that work with EXISTING code 

Lambdas allow you receive any object with a single method interface 

If the existing code has a single abstract method interface, rip it out and pass around a lambda 

List of numbers that you want to iterate through and print: 

"Ceremony" 
numbers.forEach(new Consumer<Integer>) { 
public void accept(Integer value) { 
System.out.println(num); 
} 
}); 

"Lambda" 



==================================== 
"ACT of Kindness Feature" ---> invokeDynamic (Java 7) 
Calls to Lambda Expressions are shown in byte code as invokeDynamic 
Creates less garbage then AIC 
Functions either become a static method or instance method 

Java has functional style, but is not functional 
Hybrid languages built on the concept of immutability 

====================== 
Don't mutate things in lambda expressions 

Honor Immutability: 
//total the double of values in the list example 

Top Example Feels bad! 
Smells-- 
Primitive Obsession 
Mutability 
-Stream is a very fancy iterator 
Focus on what you're doing rather than How you're doing it---> Declarative style, tell what to do, not how 

All functional code is declarative, not all declarative code is functiona 
=========================== 
Higher order functions 

Higher order functions is when you pass functions to other functions (takes a function as a parameter) 
Create objects with functions-----> Create functions from functions 
Return objects with functions-----> Return functions from functions 

Create a higher order function: 

Collection of numbers, want to total the sum of the values 

Imperative Style: 
public static int totalValues(list values) 
for int e : values 
result += e 

Now you want to exclude even values 

copy and paste to totalEvenValues, add line for mod 2 == 0 

Now you want to exclude odd values 

Copy and pasting a third time is no good! 
Chastized 

Use lambda and pass in boolean selector 
import java.util.function.Predicate; 

Predicate<Integer> is even = e -> e % 2 ==0 
System.out.println(e => e % 2 == 0); 
System.out.println(e => e % 2 != 0); 
Quietly implemented the strategy pattern 

Lambdas are Glue code----> Two lines might be too many! 

Blog post: Lambdas are Glue code on Agileprogrammer.com 

Pass a method as to a lambda: ClassName::MethodNam 
======================= 
Function Composition 

I know my code sucks. 
Why does it suck, TODAY? 

numbers.stream() 
.filter(Class::isGT3) 
.filter(e -> e % 2 ==0) 
.map(e -> e * 2) 
.findFirst()) 
.orElse(0); 

public static boolean isGT3(int number) { 
return number > 3; 
} 

Imperative example there were 8 comparison operations 
Declarative example there are ~21 operations (if it worked as it reads, but is actually 8 operations as well!) 

Functions like findFirst() cover the 80/20 rules are work like building blocks that you can chain together to accomplish 
different tasks/problems 

Code is so much easier to read and self-documents what it's actually doing 

Real problem with debugging is in lazy evaluation (function may not be executing when you think it should) 
Good modularization will help with debugging 

What point should I be concerned with performance? 
Never! (???) 
We often write unmaintainable code for better for performance 
But the code becomes so unmaintainable, and then you sacrifice maintainability and performance! 


============================ 
Lazy Evaluations 

Streams have 2 types of operations: Intermediate Operations + Terminal Operations 
Intermediate operations don't do any work until the terminal operations kick in 

Stream execution is very different than you read it 

//Execution 
numbers.stream() 
.filter(Class::isGT3) // -> 1, 2, 3 
.filter(e -> e % 2 ==0) // ->5 
.map(e -> e * 2) // 
.findFirst()) 
.orElse(0); 

Worst case scenario, performance is the same as the imperative code 
Lambda can be made concurrent by changing numbers.stream() TO numbers.parallelStream() 
That's it 

If you're not using the result (i.e., findFirst()), the rest of the methods (filter, map) are lazy, and will not do any work 

Handling exceptions? Good luck. 

Example of finding highest stock price less than 500 

Imperative example 
Functional example 

Predicates don't return a value, they return you a mechanism to obtain the value 
Coffee example: don't give you a cup of coffee, give you a coffee maker 

Functional example doesn't pass the list of symbols/prices, passes the streams 
Filter wants a predicate 

Time was about the same in the imperative vs. functional example 
Switching the functional example to parallelStream greatly improved perfomance, and didn't have to change the structure of the code 
Imperative code is very difficult to make concurrent because of the STATE and mutability 

The decision to make the code concurrent is in your hands, not the hands of the code! 


Functional so much easier to read, lowers the probability of error 

http://www.agiledeveloper.com/ 
http://www.agileprogrammer.com/
http://blog.agiledeveloper.com/
