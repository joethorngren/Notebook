# Getting Started with WireShark

### What is WireShark?

WireShark is an application that intercepts and captures traffic passing over a network

WireShark displays the details of packet data, allowing you to sort and filter the packets/traffic

WireShark is OpenSource.

![Evolution of WireShark](/res/wireshark/evolution_of_wireshark.png)

#### Network Packet Analyzer

In order to connect to the network, you need a Network Interface Card (NIC) 

The NIC card will support a specific protocol: WiFi, Ethernet, etc.

We need to make sure that the Operating System has been enabled to support packet capture (enable a driver to enable the OS to capture packets and pass them to WireShark application)

![Capturing Packets](/res/wireshark/wireshark_intro.png)

Promiscuous Mode allows WireShark to capture ALL packets on the interface, not just packets destined to and from the device it is running on

| What WireShark Does                       | What WireShark Does NOT Do                        |
|:------------------------------------------|:--------------------------------------------------|
| Captures traffic on any network interface | Does not inject traffic into the network*         |
| Provides tools for analyzing this data    | Does not take action based on the network traffic |

* Exception is name resolution

If you are measuring network performance, you need to turn off name resolution

#### WireShark Tools

- Interpretation and display (Hex/Binary + English)
- Search
- Filter
- Colorization 
- Reports
- Save, import, and export captures

#### About WireShark

Multiple programs + tools installed with WireShark

Personal Configuration: Profiles

Configure Keyboard Shortcuts

#### Privacy + Security Considerations

Wiretap Act: Driven out of Telephony industry, illegal to intercept voice calls + anything communicated over a wire/orally
Electronic Communications Privacy Act: Protects email from being intercepted

Need to be aware of what your packet capture contains:

- May contain company information
- Network + system configuration details
- Security credentials
- Personal information

Secure your packet captures! 
Get permission first! 


