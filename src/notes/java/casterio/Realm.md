
# Caster.IO Realm Course

## Lesson 1

Mobile database with built-in object framework

Realm is a replacement for SQLite in the Android platform
    
- Not built on top of or forked from SQLite at all
- Fluid API
- Do not have to write SQL queries 
- Object Database and super fast


Realm installed as a Gradle plugin:

- Add top-level build.gradle dependency:

```java
dependencies {
    classpath "io.realm:realm-gradle-plugin:version
}
```

- Apply the realm plugin via `apply plugin: 'realm-android'`

### Setting up Realm:

- RealmConfiguration.Builder() in Application class (or wherever you need/want it)

```java
public class RealmExampleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        
        // Configure Realm for the application
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
            .name("examples.realm")
            .build();
             
        // When you want a clean slate for dev/etc, use:
        // Realm.deleteRealm(realmConfiguration);
        
        // Make this Realm the default
        Realm.setDefaultConfiguration(realmConfiguration); 
    }
}
```
Inside the RealmConfiguration, you can also set up encryption, migration, etc. 

###  Creating an Object to store in Realm Database

To work with the Realm Database you need to either:
 
- Have class `extends RealmObject`
- Use Annotation above class `@RealmClass` 

There are a lot of annotations (`@PrimaryKey` for example)
 
In previous versions of Realm, you could not implement custom logic in getters/setters---> Now you can!

- In `onActivityCreated()`, initialize realm via `Realm.getDefaultInstance();` that returns the default configuraiton we set up in the `Application` class
- In ` onActivityDestroyed()`, close the resource via `realm.close();`

### Adding Objects to the Realm Database

- Long/Hard way

```java
    try {
        realm.beginTransaction();
        
        Task task = realm.createObject(Task.class);
        task.setId(UUID.randomUUID).toString();
        task.setTitle("Hello");
        task.setDescription("This is a description");
        
        realm.commitTransaction();
    } catch (Exception ex) {
        realm.cancelTransaction();
    }
```

- Shorter/Easy Way

```java
    realm.executeTransaction(new Transaction() {
        @Override
        public void execute(Realm realm) {
            Task task = realm.createObject(Task.class);
            
            task.setId(UUID.randomUUID).toString();
            task.setTitle("Hello");
            task.setDescription("This is a description");
        }
    }); 
```

`realm.executeTransaction` handles `beginTransaction()`, `commitTransaction()`, and `exceptions`

Query realm via `realm.where(Class.class).findAll()`

## Lesson 2

As of version 0.89, you no longer need to extend RealmObject.

Instead, you can annotate your class with `@RealmClass` and implement the `RealmModel` interface
 
You can use classes that extend RealmObject and classes with the annotation + interface interchangeably
 
## Lesson 3
 
`RealmChangeListener` allows you to listen for changes to the `Realm`

`RealmChangeListener` is _not_ an Anonymous Inner Class: Realm internally manages the listeners as weak references to avoid memory leaks

```java
private TextView textContent;
private Button doWorkButton;

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_main, container, false);
    textContent = (TextView) v.findViewById(R.id.text_content); 
    doWorkButton = (Button) v.findViewById(R.id.do_work_button);
     
    doWorkButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Change something
            realm.beginTransaction();
            Task task = realm.where(Task.class).findFirst();
            task.setTitle("Goodbye");
            realm.commitTransaction();
        }
    });
    
    return v;
}

@Override
public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    
    realm = Realm.defaultInstance();
    
    // Create a task
    realm.beginTransaction();
    Task task = realm.createObject(Task.class);            
    task.setId(UUID.randomUUID).toString();
    task.setTitle("Hello");
    task.setDescription("This is a description");    
    realm.commitTransaction();
    
    task = realm.where(Task.class).findFirst();
    textContent.setText(task.getTitle());
    
    realm.addChangeListener(realmChangeListener);
}

RealmChangeListener realmChangeListener = new RealmChangeListener() {
    @Override
    public void onChange() {
        // Some data has changed
        textContent.setText(task.getTitle());
        
        // Did not have to query to database to get the updated value
        // Object is memory mapped, therefore, it is already updated
        
        // If we were using a RecyclerView or a list here, 
        // all we would have to do is call invalidate
    }
}; 
```

As soon as the `doWorkButton` is clicked, the `RealmChangeListener` will get notified-- Object was automatically updated behind the scenes

To clean up after ourselves, all we need to do is:

```java
@Override
public void onDestroy() {
    super.onDestroy();
    // Prevent memory leaks
    realm.removeAllChangeListeners(); 
    realm.close();
```

