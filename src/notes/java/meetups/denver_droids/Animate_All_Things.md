### Animate All Things (except don't) 
09-21-16
Mike...?

tung.fm: Social Podcasting App

Jank: Why it happens:

- 16.67 ms
- A common cause is GC
- I/O operations on the UI thread 

- Don't make users wait (300ms is a good starting point according to Google UX research) 

android.R.integer.config_shortAnimTime (200ms)
android.R.integer.config_mediumAnimTime (400ms)
android.R.integer.config_longAnimTime (600ms)

Animations block the UI thread

Debugging: Android Developer Options ---> Drawing ---> Animator Duration Scale (don't have to recompile)

**Interpolator**: 

- Linear
- Accelerate Interpolator
- Accelerate Decelerate Interpolator
- Decelerate Interpolator
- Bounce Interpolator 

**Path Interpolators** 

**Interpolator Playground** Chet Haase 

```java
ObjectAnimator.ofFloat(View, Property, fromValue, toValue);
ObjectAnimator.ofObject(View, Property, Evaluator, fromValue, toValue);
```

Properties: 
- TRANSLATION_X
- TRANSLATION_Y
- SCALE_X
- sCALE_Y
- ALPHA

Use SCALE_X + SCALE_Y together (by themselves looks weird)

You can write your own custom properties. 
Override two methods of the Property class

**Animator Set**
- Set duration
- Set interpolator

Top Level Animators: ...? 

**Mark Allison**

Combine ObjectAnimations into AnimatorSet

##### Random Shizz
- Lint Warning for overriding onDraw() in Custom View-- Don't allocate or new up in onDraw()
- SharedPreferences commit() is synchronous or asynchronous?
- Google Play Music is great example of purposeful + natural animation (move play buttons, album covers, etc)
- Git Worktree-- Two instances of a project, side-by-side
- Transitions + AnimatedVectorDrawable are now in the support library
