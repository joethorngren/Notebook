### Little Endian vs. Big Endian

[YouTube Video](https://www.youtube.com/watch?v=MEyV7moej-k)
[YouTube Video](https://www.youtube.com/watch?v=JrNF0KRAlyo)

Where do they get their names?

![Origins](/res/java/origins_of_little_and_big_endian.png)

Words are the smallest chunk of memory that a process can handle at a time.

Are we writing Left-to-Right or Right-to-Left?

Memory addresses are tracked by the program counter. Program counter decides which bit to look at next. 

The least significant bit is the SMALLEST non-zero bit. 

Big Endian: Machine that stores the *most* significant byte first (left-to-right)

![Big Endian (Left-to-Right)](/res/java/big_endian_ltr.png)

Little Endian: Machine that stores the *least* significant byte first (right to left)  

![Little Endian (Right-to-Left)](/res/java/little_endian_rtl.png)

Remember that in [MIPS][mips]:

- 32 bits in a word
- 8 bits in a byte
- 4 bytes in a word
- The address of a 4 byte word is the address of the first byte

![Example](/res/java/little_big_endian_example.png)

[mips]:https://en.wikipedia.org/wiki/MIPS_instruction_set