# Generic Classes and Interfaces

How do generics interact with inheritance?

[Person Class](sampleCode/Person.java)

### Implementing a Generic Type

```java
public class ImplementingGenericType {
    
    public static void main(String[] args){
        Person donDraper = new Person("Don Draper", 89);
        Person peggyOlson = new Person("Peggy Olson", 65);
        Person bertCooper = new Person("Bert Cooper", 100);
        
        List<Person> madMen = new ArrayList<>();
        madMen.add(donDraper);
        madMen.add(peggyOlson);
        madMen.add(bertCooper);
        
        Collections.sort(madMen, new AgeComparator());  // See Below
        
        System.out.println(madMen);
     }
  
}
```

`Collections.sort()` takes two parameters: 

1. The list of things to sort
2. `comparator`

```java
@FunctionalInterface
public interface Comparator<T> {
    int compare(T o1, T o2);
}
```

The `Comparator` interface has a generic parameter `<T>`  and that is the type parameter of the thing we are going to sort on

Return Values: 

>`negative:` lhs < rhs
>`zero:` lhs == rhs (according to the sort order)
>`positive:` lhs > rhs
>

```java
public class AgeComparator implements Comparator<Person> {
    
    public int compare(final Person lhs, final Person rhs) {
        return Integer.compare(lhs.getAge(), rhs.getAge()); 
    }
}
```

This comparator creates a sorted list by age from youngest to oldest

- Pass Up a Type 
- ParameterType Bounds

### Passing a Parameter to a Generic Type

What is we wanted to sort the list of _Mad Men_ characters from oldest to youngest?

One approach:
```java
public class AgeComparator implements Comparator<Person> {
    
    public int compare(final Person lhs, final Person rhs) {
        return -1 * Integer.compare(lhs.getAge(), rhs.getAge()); 
    }
}
```

Multiplying by negative one will reverse the order. 

However, this is terribly inconvenient, each time we want a to compare in a different way, we need a new comparator.

Better Approach:

```java
public class ReverseComparator<T> implements Comparator<T> {

    private final Comparator<T> delegateComparator;

    public ReverseComparator(Comparator<T> delegateComparator) {
        this.delegateComparator = delegateComparator;
    }

    @Override
    public int compare(T lhs, T rhs) {
        return -1 * delegateComparator.compare(lhs, rhs);
    }
}
```

[What's going on here?](media/passing_a_parameter_to_a_generic_type.mp4)

### Type Bounds

```java
public class SortedPair<T> {
    private T first;
    private T second;
    
    public SortedPair(T left, T right) {
        first = left;
        second = right; 
    }
    
    public T getFirst() {
        return second;
    }
    
    public T getSecond() {
        return second;
    }
    
}
```

Early we implemented a `Comparator` interface---> Defined sort order

Now, we are going to implement `Comparable`, which has a single method:
```java
public interface Comparable<T> {
    public int compareTo(T o); 
}
```

`Interfaces` implement `Comparable` and say, "I'm comparable with myself"

In order to make this work with our `SortedPair` example we need to change the class declaration to:

```java
public class SortedPair<T extends Comparable> { 
    
    // same fields as above
    
    public SortedPair(T left, T right) {
        if(left.compareTo(right) < 0) {
            first = left;
            second = right; 
        } else {
            first = right;
            second = left;
        }
    }
        
    // same getters
}
```

Problem: This code produces the warning: 
> **Unchecked call to 'compareTo(T)' as a member of raw type 'java.lang.Comparable'**

The code will still compile if we changed the constructor to:
```java
if(left.compareTo(new Object) { ... }
```

**Boom** Class cast exceptions.

To fix this, we need to say:

```java
public class SortedPair<T extends Comparable<T>> { ... } 
```

## Conclusions 

#### Lessons Learned: 

- Implementing a Generic Type: 
`AgeComparator implements Comparator<Person>`
- Passing a Parameter to a Generic Type:
`Reverser<T> implements Comparator<T>`
- Type Bounds:
`SortedPair<T extends Comparable<T>>`
- JDK Comparator/Comparable
- Generics play well with Inheritance



