# Generic Methods

```java
public static void main(String[] args){
    Person donDraper = new Person("Don Draper", 89);
    Person peggyOlson = new Person("Peggy Olson", 65);
    Person bertCooper = new Person("Bert Cooper", 100);
    
    List<Person> madMen = new ArrayList<>();
    madMen.add(donDraper);
    madMen.add(peggyOlson);
    madMen.add(bertCooper);
    
    // This lesson focuses on implementing this method:
    final Person youngestCastMember = min(madMen, new AgeComparator());
    
    System.out.println(youngestCastMember);
 }
```

**Without Generics:**

```java
public static Object min(List values, Comparator comparator) {
    if (values.isEmpty()) {
        throw new IllegalArgumentException("List is empty, cannot find minimum");
    }
    
    Object lowestElement = values.get(0);
    
    for (int i = 1; i < values.size(); i++) {
        final Object element = values.get(i);
        
        if(comparator.compare(element, lowestElement) < 0) {
            lowestElement = element;
        }
    }
    
    return lowestElement;
}
```

Without a generic return type, we have to cast the returned `Object` from `min` to the type we want:
```java
final Person youngestCastMember = (Person) min(madMen, new AgeComparator());

>> Person{name='Peggy Olson', age=65}
```

We could also call this method like this:
```java
final Person youngestCastMember = 
        min(madMen, new Comparator<Integer>()) {
            public int compare(final Integer 01, final Integer 02) {
                return 0;
            }
        });
```
There is no compiler error when we use this code, but when we run it, shit blows up with a `ClassCastException` error.

**With Generics:**

```java
public static <T> T min(List<T> values, Comparator<T> comparator) {
    if (values.isEmpty()) {
        throw new IllegalArgumentException("List is empty, cannot find minimum");
    }

    T lowestElement = values.get(0);

    for (int i = 1; i < values.size(); i++) {
        final T element = values.get(i);

        if(comparator.compare(element, lowestElement) < 0) {
            lowestElement = element;
        }
    }

    return lowestElement;
}
```

**Note: Changing the return type from `Object` to `T` allows us to avoid typecasting the returned value**
