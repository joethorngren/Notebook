import java.util.Comparator;

public class AgeComparator implements Comparator<Person> {

    @Override
    public int compare(final Person lhs, final Person rhs) {
        return Integer.compare(lhs.getAge(), rhs.getAge());
    }
}
