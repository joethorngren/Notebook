# Courses + Sections

## Java Path

- [ ] Java Fundamentals: Generics
    - [ ] The What and Why of Java Generics
    - [ ] Java's Generic Collections and Friends
    - [ ] Generic Classes and Interfaces
    - [ ] Generics on Methods
    - [ ] Wildcards
    - [ ] Raw Types and Compatibility
    - [ ] Reflection
    - [ ] Advanced Topics
     