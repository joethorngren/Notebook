### Part II

- In the majority of cases, **CL** only requires a single measurement of child views within the layout pass
- **CL** is designed to be the default top-level layout container and best practice is to avoid nesting other layouts inside the ConstraintLayout.
- By keeping the layout flattened in this way it also reduces the exponential rise in measurements for the double-measurement cases because we shouldn’t need to add child layouts.
- The basic concept behind ConstraintLayout is that child views have a number of anchor points to which a constraint can be attached for the left, top, right, or bottom edges or the text baseline for widgets which contain text. 
- A constraint links an anchor point from one view to an anchor point of a sibling view, or the parent. These constraints provide similar kinds of functionality to the Relativelayout.LayoutParams attributes. 
- Similarly to RelativeLayout we can set margins to create offsets.

### Part III

- The blueprint view shows all of the currently defined constraints within the layout whereas the WYSIWYG view only shows them when it has focus (with the mouse inside its bounds).


- The blueprint view shows all of the currently defined constraints within the layout whereas the WYSIWYG view only shows them when it has focus (with the mouse inside its bounds).

![Blueprint Constraints](/res/java/android-styling_android-constraint_layout-Control-Handles.png))

- The square handles on the corners can be used to resize the widget
- The circular ones at each edge are the constraint anchor points
- The elongated rounded-rectangle in the center is the text baseline anchor point.


- The rules for creating constraints are actually pretty simple: 
    - You can only create a constraint from a horizontal anchor point (left or right; or the baseline anchor point) to a horizontal anchor point on another widget or the parent ConstraintLayout
    - You can only create a constraint from a vertical anchor point (top or bottom) to a vertical anchor point on another widget, or the parent ConstraintLayout.


- ">>>" Symbol:

-   ">>>" === Wrap Content
- "|---|" === Fixed Size
- The third state is called “Any Size” and means that the View will size to fit the available space. This is vaguely analogous with match_parent (which is not supported by ConstraintLayout – if you specify it you will actually get Any Size behaviour). Any Size behaviour is actually closer to combining layout_width="0dp" and layout_weight="1" when using a weighted LinearLayout. In our simple example this will behave exactly like match_parent but, as we’ll see later on in this series, Any Size will not necessarily match the parent size hence the decision not to support match_parent because the name is actually misleading in a ConstraintLayout context