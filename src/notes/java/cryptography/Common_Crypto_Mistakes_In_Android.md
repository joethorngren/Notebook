[Video Link](https://tozny.com/blog/video-common-crypto-mistakes-in-android/)
[Article Link](https://tozny.com/blog/encrypting-strings-in-android-lets-make-better-mistakes/)

Plain Text ---> AES ---> Looks Encrypted To Me!

### You need to do cryptography... What next?

#### Step 1: Go to the API documentation

**Problem**: 

- The standard API is generic for all possible crypto
- The API docs have zero information about how to use it

#### Step 2: Find a good library

**Problem**: 

- Ok, but that's not exactly what I want
- How you use crypto is highly dependent on your application

#### Step 3: Use web-based code example

**Problem**: 

- They're mostly wrong (see problem 1)

#### Why is the API so hard? CHOICES!

- What type of data?
    - Strings
    - Files
    - Pictures
    - Binary
    
- How will you handle the keys?
    - Password-based
    - Randomly generated
    - Public/Private

- What algorithm?
    - Symmetric
    - Asymmetric
    - AES
    - RSA

- What key size?
    - 128
    - 256
    - 2048
    
- What mode?
    - ECB
    - CBC
    - GCM
    
