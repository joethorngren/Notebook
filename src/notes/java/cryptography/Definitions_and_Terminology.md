Definitions and Terminology

**Encryption** = the process of disguising a message so as to hide the information it contains;this process can include both encoding and enciphering 
**Protocol**= an algorithm, defined by a sequence of steps, precisely specifying the actions of multiple parties in order to achieve an objective.
Plaintext = the message to be transmitted or stored.
**Ciphertext** = the disguised message.
**Alphabet** = a collection of symbols, also referred to as characters.
**Character** = an element of an alphabet.
**Bit** = a character 0 or 1 of the binary alphabet.
**String** = a finite sequence of characters in some alphabet.
**Encode** = to convert a message into a representation in a standard alphabet, such as to the alphabet {A, . . . , Z} or to numerical alphabet.
**Decode** = to convert the encoded message back to its original alphabet and original form— the term plaintext will apply to either the original or the encoded form. The process of
encoding a message is not an obscure process, and the result that we get can be considered equivalent to the plaintext message.
**Cipher** = a map from a space of plaintext to a space of ciphertext.
**Encipher** = to convert plaintext into ciphertext.
**Decipher** = to convert ciphertext back to plaintext.
**Stream cipher** = a cipher which acts on the plaintext one symbol at a time.
**Block cipher** = a cipher which acts on the plaintext in blocks of symbols.
**Substitution cipher** = a stream cipher which acts on the plaintext by making a substitution
of the characters with elements of a new alphabet or by a permutation of the characters
in the plaintext alphabet.
**Transposition cipher** = a block cipher which acts on the plaintext by permuting the
positions of the characters in the plaintext