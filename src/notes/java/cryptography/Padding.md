There are five confidentiality modes of operation for Symmetric Key Block Cipher Algorithms:

- ECB: Electronic Codebook Mode     ---> Requires Padding
- CBC: Cipher Block Chaining Mode   ---> Requires Padding
- CFB: Cipher Feedback Mode         ---> Requires Padding
- OFB: Output Feedback Mode         ---> No Padding
- CTR: Counter Mode                 ---> No Padding

For ECB and CBC modes, the total number of bits in the plaintext must be a multiple of the block size
For CFB mode, the total number of bits in the plaintext must be a multiple of a parameter

A transformation is of the form: Algorithm/Mode/Padding





