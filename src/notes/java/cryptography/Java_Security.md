### Java Security Part 1: Crypto Basics
https://www.ibm.com/developerworks/java/tutorials/j-sec1/j-sec1.html#mdexample

#### How the Java platform facilitates secure programming

The Java programming language and environment has many features that facilitate secure programming:

- **No pointers**, which means that a Java program cannot address arbitrary memory locations in the address space.
- **A bytecode verifier**, which operates after compilation on the .class files and checks for security issues before execution. For example, an attempt to access an array element beyond the array size will be rejected. Because buffer overflow attacks are responsible for most system breaches, this is an important security feature.
- **Fine-grained control over resource access** for both applets and applications. For example, applets can be restricted from reading from or writing to disk space, or can be authorized to read from only a specific directory. This authorization can be based on who signed the code (see The concept of code signing) and the http address of the code source. These settings appear in a java.policy file.
- **A large number of library functions** for all the major cryptographic building blocks and SSL (the topic of this tutorial) and authentication and authorization (discussed in the second tutorial in this series). In addition, numerous third-party libraries are available for additional algorithms.

#### What are secure programming techniques?

- **Storing/deleting passwords**: If a password is stored in a Java String object, the password will stay in memory until it is either garbage collected or the process ends. If it is garbage collected, it will still exist in the free memory heap until the memory space is reused. The longer the password String stays in memory, the more vulnerable it is to snooping.
- **Smart serialization**: When objects are serialized for storage or transmission any private fields are, by default, present in the stream. So, sensitive data is vulnerable to snooping. You can use the transient keyword to flag an attribute so it is skipped in the streaming.


#### Terms:

- **Message digests**: Coupled with message authentication codes, a technology that ensures the integrity of your message.
- **Private key encryption**: A technology designed to ensure the confidentiality of your message.
- **Public key encryption**: A technology that allows two parties to share secret messages without prior agreement on secret keys.
- **Digital signatures**: A bit pattern that identifies the other party's message as coming from the appropriate person.
- **Digital certificates**: A technology that adds another level of security to digital signatures by having the message certified by a third-party authority.
- **Code signing**: The concept that a trusted entity embeds a signature in delivered code.
- **SSL/TLS**: A protocol for establishing a secure communications channel between a client and a server. Transport Layer Security (TLS) is the replacement for Secure Sockets Layer (SSL).


#### Java security, Part 2: Authentication and authorization
https://www.ibm.com/developerworks/java/tutorials/j-sec2/j-sec2.html

Elements of authentication
Authentication is based on one or more of the following elements:
What you know. This category includes information that an individual knows that is not generally known by others. Examples include PINs, passwords, and personal information such as a mother's maiden name.
What you have. This category includes physical items that enable individual access to resources. Examples include ATM cards, Secure ID tokens, and credit cards.
Who you are. This category includes biometrics such as fingerprints, retina profiles, and facial photographs.
Often, it isn't sufficient to use only one category for authorization. For example, an ATM card is generally used in combination with a PIN. Even if the physical card is lost, both the user and the system are presumably safe, since a thief would have to know the PIN to access any resources.
Elements of authorization
There are two fundamental ways of controlling access to sensitive code:
Declarative authorization can be performed by a system administrator, who configures the system's access (that is, declares who can access which applications in the system). With declarative authorization, user access privileges can be added, changed, or revoked without affecting the underlying application code.
Programmatic authorization uses Java application code to make authorization decisions. Programmatic authorization is necessary when authorization decisions require more complex logic and decisions, which are beyond the capabilities of declarative authorization. Since programmatic authorization is built into the application code, making programmatic authorization changes requires that some part of the application code be rewritten.



