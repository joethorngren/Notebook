Here are the three main packages in the APIs:

- **javax.crypto**: This package provides the classes and interfaces for cryptographic applications implementing algorithms for encryption, decryption, or key agreement.
- **javax.crypto.interfaces**: This package provides the interfaces needed to implement the key agreement algorithm.
- **javax.crypto.spec**: This package provides the classes and interfaces needed to specify keys and parameter for encryption.