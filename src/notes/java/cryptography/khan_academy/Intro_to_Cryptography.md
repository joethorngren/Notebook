Cryptography begins when we abandon physical locks and use ciphers instead.

Think of a cipher as a virtual lock.

Caesar Cipher is a substitution cipher.

![Caesar Cipher Example](/res/java/caesar_cipher_example.png))

Caesar Cipher was broken with "frequency analysis".
![Frequency Analysis](/res/learn/letter_frequency.analysis.png))
 
A strong cipher disguises your fingerprint. 
To make a lighter fingerprint, you need to flatten the distribution of letter frequencies.

Polyalphabetic Cipher: 

- Use a key to select which alphabet is used for each letter of the message
- Use each alphabet in turn
- Repeat from start after end of key is reached ---> Longer key makes it harder to break

![Polyalphabetic Cipher Example](/res/java/polyalphabetic_cipher_example.png))

One-Time Pad:

The One-Time Pad is just a simple variation on the Polyalphabetic Cipher. 
It starts with a random sequence of letters for the standard text (which is the key in this case). 
Suppose for example one uses RQBOPS as the standard text, assuming these are 6 letters chosen completely at random, and suppose the message is the same. 
Then encryption uses the same method as with the Beale Cipher, except that the standard text or key is not a quotation from English, but is a random string of letters.


The term "perfect" in cryptography also means that after an opponent receives the cipher text he has no more information than before receiving the cipher text.
