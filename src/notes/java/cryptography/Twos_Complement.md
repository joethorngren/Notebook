https://www.youtube.com/watch?v=9W67I2zzAfo
https://www.youtube.com/watch?v=Hof95YlLQk0

3-Bit Number System (Unsigned):

| Binary | Decimal |
|:-------|:--------|
| 000    | 0       |
| 001    | 1       |
| 010    | 2       |
| 011    | 3       |
| 100    | 4       |
| 101    | 5       |
| 110    | 6       |
| 111    | 7       |

We want to convert this to a **signed** number system.

#### Sign + Magnitude: 

- Divide our number space in half, half positive, half negative.
- The positive numbers are represented by a leading 0
- The negative numbers are represented by a leading 1
- The first bit represents the sign of the number
- The remaining bits represent the magnitude of the number 

Drawbacks:

- Two zeros
- Difficult to implement in hardware

"Not an ideal way to do it"

| Binary | Decimal |
|:-------|:--------|
| 000    | 0       |
| 001    | 1       |
| 010    | 2       |
| 011    | 3       |
| 100    | -0      |
| 101    | -1      |
| 110    | -2      |
| 111    | -3      |

#### Ones Complement

When we have an original value, to get the negative we simply flip the bits

001 ==  1  == Positive
110 == -1  == Negative

Drawbacks:

- Two zeros
- Difficult to implement in hardware

| Binary | Decimal |
|:-------|:--------|
| 000    | 0       |
| 001    | 1       |
| 010    | 2       |
| 011    | 3       |
| 100    | -3      |
| 101    | -2      |
| 110    | -1      |
| 111    | -0      |


#### Twos Complement

When we have an original value, to get the negative we:

- Flip bits
- Add one

Starting with zero:

- Flip the bits

111 
 
- Add one 

   111
   + 1
= 1000

| Binary | Decimal |
|:-------|:--------|
| 000    | 0       |
| 001    | 1       |
| 010    | 2       |
| 011    | 3       |
| 100    | -4      |
| 101    | -3      |
| 110    | -2      |
| 111    | -1      |

Takeaways + Patterns: 

- -1 will always be all ones
- The smallest negative number will always be represented by a leading 1 and the rest 0s
- The largest positive number will always be represented by a leading 0 and the rest 1s

Benefits:

- No two zeros! 
- Implement subtraction and addition as simple addition (Major hardware advantage)

Overflow Condition: 

Add 3 + 2

  011
+ 010
= 101

101 == -5

Add -4 - 3

  100
+ 101
= 001         *Ignore the carry

001 == 1


