## Bitwise AND (&)

![Bitwise AND](/res/java/bitwise_operators_and.png)

![Bitwise AND Example](/res/java/bitwise_operators_and_example.png))

## Bitwise OR (|)

![Bitwise OR](/res/java/bitwise_operators_or.png))

![Bitwise OR Example](/res/java/bitwise_operators_or_example.png))

## Bitwise XOR (^)

![Bitwise XOR](/res/java/bitwise_operators_xor.png))

![Bitwise XOR Example](/res/java/bitwise_operators_xor_example.png))

## Bitwise NOT (~)

![Bitwise NOT](/res/java/bitwise_operators_not.png))

![Bitwise NOT with Twos Complement](/res/java/bitwise_operators_not_with_twos_complement.png))


[Youtube Video](https://www.youtube.com/watch?v=lKGQU-d1Y6E)