# `locate` vs. `find`

`find`   

- Searches a directory structure for requested files
- `print` - the default action and displays matches
- `ls` - displays full details on matches
- `exec` - allows a command to be run against each
- `ok` - can be used when a confirmation prompt is desired

`locate`

- High-speed and low impact searching
- Searches index of actual filesystem
- Index updated each night by default
- `locate` won't know about recently added/deleted files until database is rebuilt

Both commands perform the same function

## Locate

Command to update locate database:
`$ sudo updatedb`
     
The `locate` database file is located in `/etc/updatedb.conf`

Inside of the `updatedb.conf` file, there is a variable titled `PRUNEPATHS` that specifies directories that will not be searched

`locate`, because it searches its database, may not find newly created/deleted files

**OPTIONS** 

      -0, --null
              Use ASCII NUL as a separator, instead of newline.
      -A, --all
              Print only names which match all non-option arguments, not those matching one or more non-option arguments.

       -b, --basename
              Results are considered to match if the pattern specified matches the final component of the name of a file as listed in the database.  This final component is usually referred to as the `base name'.

       -c, --count
              Instead of printing the matched filenames, just print the total number of matches we found, unless --print (-p) is also present.

       -d path, --database=path
              Instead of searching the default file name database, search the file name databases in path, which is a colon-separated list of database file names.  You can also use the environment variable LOCATE_PATH  to  set  the
              list  of  database  files  to search.  The option overrides the environment variable if both are used.  Empty elements in the path are taken to be synonyms for the file name of the default database.  A database can be
              supplied on stdin, using `-' as an element of path. If more than one element of path is `-', later instances are ignored (and a warning message is printed).

              The file name database format changed starting with GNU find and locate version 4.0 to allow machines with different byte orderings to share the databases.  This version of locate can automatically recognize and  read
              databases produced for older versions of GNU locate or Unix versions of locate or find.  Support for the old locate database format will be discontinued in a future release.

       -e, --existing
              Only  print  out  such names that currently exist (instead of such names that existed when the database was created).  Note that this may slow down the program a lot, if there are many matches in the database.  If you
              are using this option within a program, please note that it is possible for the file to be deleted after locate has checked that it exists, but before you use it.

       -E, --non-existing
              Only print out such names that currently do not exist (instead of such names that existed when the database was created).  Note that this may slow down the program a lot, if there are many matches in the database.

       --help Print a summary of the options to locate and exit.

       -i, --ignore-case
              Ignore case distinctions in both the pattern and the file names.

       -l N, --limit=N
              Limit the number of matches to N.  If a limit is set via this option, the number of results printed for the -c option will never be larger than this number.

       -L, --follow
              If testing for the existence of files (with the -e or -E options), consider broken symbolic links to be non-existing.   This is the default.

       --max-database-age D
              Normally, locate will issue a warning message when it searches a database which is more than 8 days old.  This option changes that value to something other than 8.  The effect of specifying a negative value  is  unde‐
              fined.

       -m, --mmap
              Accepted but does nothing, for compatibility with BSD locate.

       -P, -H, --nofollow
              If  testing for the existence of files (with the -e or -E options), treat broken symbolic links as if they were existing files.  The -H form of this option is provided purely for similarity with find; the use of -P is
              recommended over -H.

       -p, --print
              Print search results when they normally would not, because of the presence of --statistics (-S) or --count (-c).

       -r, --regex
              The pattern specified on the command line is understood to be a regular expression, as opposed to a glob pattern.  The Regular expressions work in the same was as in emacs and find, except for the fact that  "."  will
              match  a newline.  Filenames whose full paths match the specified regular expression are printed (or, in the case of the -c option, counted).  If you wish to anchor your regular expression at the ends of the full path
              name, then as is usual with regular expressions, you should use the characters ^ and $ to signify this.

       -s, --stdio
              Accepted but does nothing, for compatibility with BSD locate.

## Find

`find` searches the directory structure in real time  

Breaking down the `find` command by sections:

        |-1-|     |-2-|     |------3------|          |--4--|
    $   find        .        -name    \*.c           -print
    
1. The command itself
2. Where you want to search (in this case, current working directory)
3. What files you are searching for (may contain additional parameters)
4. Execute something (print is the default option)

**Examples:**

    $ find / -name "*song*"                                       // Find any file with the pattern "song" in the name
   
    $ find / -name "*song*" -user dayfun                          // Same as above, but further specifies that the files are owned by user "dayfun"
    
    $ find / -name "*song*" -user dayfun -exec ls -l {} \;        // Same as above, executes 'ls -l' and stores the result in the {} brackets 
    
In the last example, anything that is found by: 

`$ find / -name "*song*" -user dayfun`

will be placed inside the {}, so if `find` finds the files: 

    /home/dayFun/Music/Song1.mp3
    /home/dayFun/Music/Song2.mp3
    /home/dayFun/Music/Song3.mp3
    
Then the end result will be: 

    ls -l /home/dayFun/Music/Song1.mp3
    ls -l /home/dayFun/Music/Song2.mp3
    ls -l /home/dayFun/Music/Song3.mp3
 
 