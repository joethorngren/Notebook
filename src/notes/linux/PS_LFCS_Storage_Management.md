## Part I: Partitioning Disks
  
- You can format disks with fdisk, gdisk, and/or parted

#### Linux File System Overview

Disk: Physical or virtual

Two Types of Partition Tables:

- MBR:
    - "Old School"
    - Maximum Partition Size of 2TB
    - Allowed to create a maximum of 4 primary partitions
        - One of these primary partitions can be an extended partition
        - In the Extended partition, we can start creating any number of logical partitions (limited by driver)
- GUID (Globally Unique Identifier Disc)
    - Maximum size of 8 Zetabytes
    - Allowed to create a maximum of 128 primary partitions
        - LVM == Logical Volume Management
        
![Linux File System Overview](/res/linux/PS_linux_storage_management__linux_file_system_overview.png)

#### Partitioning with fdisk 

