[Presentation Link](https://www.youtube.com/watch?v=DeO2J3DnZqU)

Desktop Environments--Overall Look and feel of the interface (start menu, icons, task bar)--
     -RedHat Linux:
          -GNOME
               Must user window manager that is "GNOME aware" (i.e., MetaCity, Sawfish, Windowmaker)
          -KDE 

Window Managers--Controls the way X clients are displayed, moved, and resize
     -GNOME default: MetaCity (used to be Sawfish)
     -KDE default: Kwin 

Display Managers--Initial Login Screen
     -GDM (Gnome Display Manager)
     -KDM (K Desktop Manager)
