
![What's Refactoring Again?](/res/venkat/whats_refactoring_again.png)

![Strike a Balance](/res/venkat/strike_a_balance.png)

> "I'm a huge fan of being fearful, honestly, because if you're fearful, that tells us that we care about what we're doing.
> If you're fearful, that tells us pay that we are going to pay attention to what you're doing
> If you're fearful, that tells us that you care about the outcome of our effort."

Why Fear Refactoring?

- What if I break something that worked?
    - Have automated tests to validate your change
- Is my change worse than the original code?
    - Get feedback from a respectable colleague 
- We hate being embarrassed, it's easy to leave things as they are
    - Get over it  


  