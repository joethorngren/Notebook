### Pomodoro Tools

Rather than just timing yourself with a stopwatch, you can now use a multitude of online and mobile apps that will take care of your Pomodoro periods for you. I personally use my own app, AceTimer ( www.52aces.com/acetimer ), on my computer and Clear Focus on my phone. If you want to try another tool, another one I’ve used successfully is Tomato Timer ( www.tomato-timer.com ). Using an app in this manner will save you the time and brainspace required to use a simple stopwatch and give you audio cues (such as a buzzer) to remind you when to stop.

### Pomodoro Queues

Another easy but effective concept you can use is what I call the Pomodoro queue. It’s very simple and straightforward, but having one in place will help tremendously when you’re first starting out and want to build a Pomodoro habit. Decide on several activities (I recommend no more than three if you’re just starting out) and put them into a list of some kind. For each subject, figure out a beneficial way to either learn about it or work on something related to it in a Pomodoro time frame.

This will give you a linear, easy-to-follow list of items that will keep you on track. I also recommend that you put the most difficult subjects/activities first. The more you do this last part, the more you’ll start to realize that the difficulties you perceive aren’t that bad, and subsequent Pomodoros will be much easier (even pleasurable) to deal with.

My personal technique for using Pomodoro queues is to place the subjects I want to study for the day into a Google Docs spreadsheet (you can use Excel or even a word processor, but I prefer this setup for its cloud-based access and flexibility). The format looks like this:  

![Example Pomodoro Queue](/res/learn/pomodoro_queue_example.png))

I’m currently in my “Writing” Pomodoro, and I’ve been using this format to write this book. It’s an incredibly easy and powerful way to ensure that you focus all of your energy on a certain task, particularly because there’s a reward waiting at the end. I like to watch YouTube videos (especially documentaries) whenever my Pomodoro periods are up, but use whatever you think will be relaxing and rewarding.

You should view each one of those subjects as possibly having multiple Pomodoro sessions. For example, in my own system I usually will do at least two Pomodoros of writing before moving on to whatever is next that day. The quantity of Pomodoro blocks per subject is highly dependent on my schedule, but I make sure I do at least one for each topic.

You can create something similar, although I recommend you hold off on doing five Pomodoros until you’ve had some time to build up the habit. You’re better off doing one or two at the beginning and going up from there. 

### Use Google Docs Forms to Track Yourself

Online forms used to be things that were understood exclusively by those who worked with web technology, such as web designers and developers. However, Google has changed all of that with the introduction of Docs and its accompanying Forms document type. Forms allow you to quickly and easily create forms and, as an added bonus, the responses get recorded in a spreadsheet. To top that all off, with the click of a menu button the responses can be graphed so you can view a statistical representation.

I’ve used this for various things over the years, but you can use it for keeping track of what you’re doing while you learn. One idea you can use is the Pomodoro tracker, which is simply a form that you use to record what you’ve done with each focused work period you undertake.

![Example Google Form](/res/learn/google_form_sample.png))

https://support.google.com/docs/answer/87809?hl=en
