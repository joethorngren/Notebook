We’ve previously explored how passive note taking is a giant waste of time and I want to emphasize again that just taking notes is counter-productive. The practice of writing in the margins of a book, a practice known as marginalia , is equally wasteful. **Instead, what you should be doing is taking notes with the express intention of converting them into flashcards.**

--- 

While watching lectures, take notes and convert those into flashcards. When an exam or quiz comes up, use that material as a basis for flashcards as well - in this case, they often have done all the work for you by defining the questions and answers.

---

