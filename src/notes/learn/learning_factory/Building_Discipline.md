Step One: Pay Attention to Your Routines

If you aren’t sure how to start, I’ll give you some ideas about what to evaluate:


1. What topics get you excited whenever you come across them?
2. When do you usually wake up in the morning?
3. What time do you normally go to bed?
4. How do you normally react when you know you have to learn something new?
5. How often do you exercise?
6. What kinds of foods do you eat on a regular basis?
7. What types of media do you find yourself consuming most often (books, music, video games, etc.)? On average, how much money are you spending each day?
8. What are you spending your money on? 


**Make sure you're keeping track of what you're observing**

You want to figure out these key points about your life:


1. What cues throw you off your discipline (e.g., text messages, social networking, etc.)?
2. What you want to learn (e.g., neuroscience, lockpicking, programming, painting, etc.)?
3. How do you approach learning at the moment (e.g., reading, TV, movies, blogs, etc.), if at all?
4. When will you be able to set aside time each day for learning (e.g., mornings, afternoons, evenings)?
5. What can learning do to help you in your daily life (e.g., make friends, start a business, get a job, etc.)? 


One point that absolutely must be accounted for is finding the cues that destroy your focus. Everyone has different distractions in their lives and yours will inevitably be unique.

Step Two: One Thing Everyday

It really doesn’t matter what the specific habit is, as long as it can somehow aid your learning. Physical exercise has been shown to have wide-reaching effects on mental health, mood and cognitive performance, so I’d recommend you incorporate that into your routine. 

Once you’ve selected this one thing, make a point of doing it in the same form every single day for the next week.

Step Three: Gradually Increase the Difficulty and Complexity

Once you’ve done that one small thing every day for at least a month (this has been, in my own experience, the most effective length of time), make it more difficult. If you’ve elected to use the example of five push-ups to begin with, bump that number up to at least 10 push-ups. In addition to the routine you’ve already built, begin doing one other small thing every day, preferably at the same time. If you’re doing something like push-ups, find a mental exercise that you can easily commit to each day at a specific time.

What you absolutely must avoid is slamming a huge amount of discipline down your throat at once. I can tell you from personal experience that this will yield only short-term results that quickly get buried under an inevitable burnout. Your brain needs time to adjust to change, and will actively resist dramatic changes that you try to force on it.

Exercise: Create a Kata

I don’t know what you want to learn, but I can give you some examples from my own life. When I started learning to program, I realized that knowing how and when to write loops was a fundamental skill every programmer needs to possess. So every day I wrote simple loops over and over again for at least ten minutes. At first it was simple stuff like counting to ten, but it eventually evolved into more sophisticated operations such as searching data structures.

Now I can write a loop without having to think about the syntax, and instead focus on the inner components of the loop. If I couldn’t manage this basic programming task, I wouldn’t be a programmer.

Step Four: Challenge Yourself

By the time you’ve reached this step, you should have established a decent base of discipline to work with. You may still be a long way from top-notch in the self-control department, but you’ve created the foundation you’ll need for building solid routines. Now that you’re here, it’s time to figure out some habits that can begin to have real impact in your life.

Keep in mind what we discussed earlier about deliberate practice and how your brain reacts to difficulty. Don’t try to become a discipline Rambo overnight. You’ll just end up shooting yourself in the foot via burnout and frustration. It’s especially important to keep this in mind for physical activities, as overwork is one of the most common causes of injury for beginning exercisers.

Another thing you should focus on is a shift from specific number of repetitions to time-defined processes. It is a good idea to start out using simple routines with small numbers of defined repetitions, but once you’ve gotten further into the process, it becomes far more important to simply work on a skill or subject for set periods of time.

For mental activities such as writing, challenge yourself to focus exclusively on that activity in short, distraction-free bursts. I highly recommend you try using the Pomodoro technique.

Step Five: Define Some Rewards

It’s also important to set stopping times every day. This works on several levels: you have that mild stress we talked about being applied to your work, and you get to have guilt-free downtime. This gives you a nice boost of productivity and helps to prevent burnout. This part is absolutely, 100% non-negotiable. Without rewarding yourself occasionally, you will fail while building your routines. Just be careful not to fall into the trap of rewarding yourself excessively.

Step Six: Evaluate Your Progress

Read through your notes at this point and start to pick through the routines your routines that you've inserted into your life. How consistent are you with each one? Do you feel that you're focusing sufficiently on the problem areas in your routines? What impact has the randomness of everyday life had on your ability to maintain consistency?

Do your best to honestly appraise yourself and making adjustments to your routines that will maximize your learning. This may include rearranging your schedule, defining new rewards or, if you find that your goals have changed, shifting your routines so that you’re exploring new skills or subjects.

Step Seven: Remove Your Crutches

Some people are really intent on tracking everything in their lives. There’s even a whole “movement” associated with collecting data about yourself on a regular basis called Quantified Self.

Keep track of what you’re doing, but don’t go overboard.

If you want to continue tracking, by all means do so. I personally try to be as self-reliant as I can, and part of that means I don’t want to rely on structured tracking and/or apps to ensure that I’m doing the right thing in the long-term. So after I’ve worked on incorporating a new routine into my life for between one and three months, I’ll either stop tracking it completely or tone down the level of detail. This has made me far more self-sufficient, and I’ve found that I have no trouble keeping on the right path once I’ve done something long enough. On top of that, I’ll define a goal I need to reach that will require me to keep a pace that is either the same or similar to what I had before. I’ll then set a date for checking my progress on that goal, and if I’m not making adequate progress then I will start tracking again.

Most of the time, I don’t need to do that. But when I do, I hop back on the tracking wagon with the same enthusiasm I had before. Once I’ve done that for long enough, I can try going off again to see how well I do. Rinse and repeat.

Failures

There will be times when, for one reason or another, you’ll fail to maintain discipline. It can (and most likely will) happen at any stage of this process. If and when this happens, keep this in mind: it’s not the end of the world. Everyone screws up and you shouldn’t expect yourself to be perfect as you work to improve yourself. Take notes on what you did wrong and how you can do better next time, then start the next day fresh.

I will say this, though: don’t use this as an excuse to make the same mistakes over and over again. Occasional slip-ups and cheat days are to be expected, but not regular mess ups. If you’re finding your discipline slipping on a regular basis I suggest you take an inventory and figure out why you’re not making progress.

Quiz


1. What type of tasks should you be using to build your discipline at the earliest stages?
2. What should you define in order to keep yourself motivated?
3. What technique can you use to stay focused for short periods of time?
4. What should you do if you allow your discipline to slip?
5. What should you be doing on a regular basis to further develop your discipline?

