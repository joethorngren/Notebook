Let’s walk through an example so you can clearly understand this idea. Let’s say you want to learn about human memory. Since you’re reading this book, I’m assuming this to be the case and truly hope you explore it more. Anyway, let’s look at how you could use the model of object-and-supplement flashcards in this scenario. 

I’ll use the cognitive science concept of procedural memory (a type of implicit memory ) as a testbed for this model. 

First, you should add an object note for the concept itself. My deck has a “Definitions” note type that is used for such things, and it allows me to create several identifier cards for whatever concept I want to learn. Definition notes also include images and pictures, which takes care of the “vivid” piece of the puzzle. This note type has fields such as the following: 

- Name 
- Definition
- AKA (if the concept has another name it is commonly known as) 
- Simplification (a field for less rigorous, easier to understand definitions) 
- Opposite (for any concepts that acts as opposites) 

Your object note type may be very different, but the idea should be the same: use the object as a way to create cards with key features of the concept you want to learn. Second, create a few basic trivia cards, such as: 

Front: What type of memory is used to recall “how-to” information about tasks?
Back: Procedural memory 

Front: What sort of memory would you be utilizing when riding a bike?
Back: Procedural memory 

Front: What category of memory does procedural memory belong to? 
Back: Implicit memory 

Third, create a few cloze cards as well: 

`{{c1::Procedural}} memory guides the {{c2::processes}} we perform and most frequently resides {{c3::below}} the level of conscious {{c4::awareness}}.`

With this three-pronged approach, you’re creating several chunks that can be used to link to higher concepts. The object card gives you a general idea about what the concept is and what it entails, while the trivia and cloze cards give you more contextual chunks. The trivia cards are especially important in this mix, as the questions you formulate act to test your overall knowledge of the concept in relation to its overall subject. This combination has been, in my experience, a highly effective way to retain both the factual knowledge surrounding a concept and some additional context to aid in recall.