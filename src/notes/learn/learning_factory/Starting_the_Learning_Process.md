![Funnels To Flashcards](/res/learn/funnels_to_flashcards.png)))

Once you’ve (hopefully) taken at least a small amount of time to figure out the why , you need to start working on the how . Determine sources of information that you’ll get the most benefit from in the shortest amount of time. There are a variety of ways to accomplish this, but here is the process I use:

1. Go to Wikipedia and type in the most general term you can think of related to what you want to learn. For example, if you’re trying to learn how to program, go to the Wikipedia page for “computer programming.”
2. Read through the article and, as you’re reading, start a list of topics that show up consistently. To continue using the example of computer programming, you’d likely want to include algorithms, variables, programming languages and so on. Keep this list handy, but don’t do anything with it yet.
3. Look around for online communities that focus on the topic you’re interested in. Probe around for relevant subtopics, books, online classes or anything else that might be useful for a beginner. Seek out the recommendations for people that have experience with the subject, and heed warnings about bad learning materials that might be a waste of your time.
4. Create a booklist with as clear of a progression as you can create. Start with books that are going to be easier to understand and give you the basic building blocks you’ll need later on, then work your way towards advanced texts.

## Learning Funnels

The first piece of your learning system needs to be a series of what I call learning funnels . These provide you with a reference for what you’re learning and methods for learning it. It’s a great way to keep yourself from becoming distracted by every interesting-looking book, MOOC or other material that you come across. This is something I struggled with tremendously, as I find myself drawn to a wide variety of subjects.

There’s a great deal of value in being well-versed in many subjects, but you’ll find that spending concentrated chunks of time focusing on one thing at a time is a much better way to expose yourself to each subject. The trap that most people fall into is that they become habitual generalists - they simply see something interesting and dive into it. Don’t do this.Instead, use your previously determined goals to create a linear progression of learning that will ultimately lead to exponential growth in your knowledge.

### Article Funnel

As I mentioned at the beginning of the chapter, spend some time on Wikipedia and the web exploring articles related to the subject you want to learn. My personal process is to start with Wikipedia, then move on to using Reddit (reddit.com), which usually has plenty of links to relevant articles and discussions. I generally look for a sub-forum (called a subreddit) about the subject and go from there (for example, a subreddit for learning to program exists called /r/learnprogramming).

Your other sources may vary, but I cannot recommend starting with Wikipedia highly enough. The articles can be verbose and hard to decipher at times, but they are very thorough and can provide a great springboard for learning the ins and outs of a subject. The sheer amount of links you’ll find in a base category article (pages such as “Spanish”, “Language” and “Computer programming”) alone is enough to keep your learning funnel filled for the foreseeable future. Once you find some articles you feel are relevant, send the links to the email address you just set up.

You should now have a nice little list of items to work through. Do yourself a favor and don’t send 50 links on the first day. As mentioned before in the section on discipline, you should start small and progress from there. Limit the number of articles in the email funnel to 10 when you’re first getting started.

Alternatives to Email: Pocket, EverNote, & Pushbullet

### Book Funnel

--Shoutout to GoodReads--

Make a list of books related to your subject. Keeping in mind my previous advice about recommendations from industry veterans, assemble a small list of books. I previously mentioned Reddit as a great place for finding articles, and it turns out to be a pretty good place to find books as well. Many subreddits even have recommended reading lists that you can reference.

Hold off on reading those books for now. You don’t want to start going through your list until you’ve learned how to effectively transfer the information in the pages you read into your long-term memory. For the moment, just focus on picking out some books and structuring your reading list how I describe in the following paragraphs.

After trying many different methods, I’ve found that the best way to get started is by reading small, simple texts before progressively moving on to larger, more complicated volumes. This gives you a chance to get a taste of a subject and have some small wins without burning you out.

There are several ways to structure your reading list, but I’ll focus on the two that are the most applicable to the highest number of people. The first is a list for someone that has a single subject they want to be an absolute master of, and the other is for someone that wants to learn multiple subjects.

If you want to learn just one subject the structure is pretty straightforward. Start with small, easy-to-digest volumes at the top, and end with difficult, dense books at the bottom. The books at the top should be of the “For Dummies” variety - simple guides designed for the layperson. Aside from the “For Dummies” books, you can usually find simplified introductions for whatever subject you’re trying to pick up.

This first approach is well-suited to people who already have a degree of experience with something and are looking to upgrade their understanding and/or skills. It’s also a good method if you’re strapped for time and can’t afford too much dallying.

A good alternative to the “For Dummies” series that covers many different subjects is the “A Very Short Introduction” series by the Oxford University Press.

For those of you that, like me, are wanting to explore more than one subject, then the structure is a little more involved. Rather than just one long list for one topic, build a large list with chunks of subjects that you’re interested in. Chunks are made up of three to five books, all of roughly the same degree of difficulty and length.

The chunks then depend on where you are in your learning process. Subjects that you’re entirely unfamiliar with should obviously have initial chunks that are of the “For Dummies” variety. Assuming that you found the subject interesting and/or useful, your later chunks should be more advanced.

The idea here is to focus intensely for a short period of time on each subject. If you have only one book on a subject in your list, then chances are you won’t get much depth to your knowledge 37 . However, if you’re looking to become well-versed in a subject (but not necessarily a master of it yet) breaking your learning into chunks is a nice middle ground.

Another key concept to keep in mind is that once you’re past the beginning stages you’ll need to start reading books about specific areas of the subject. It’s easy to believe you’re well-versed in something after reading a book that gives a sweeping overview, but to really understand it you’ll need to start diving into the various branches of knowledge.

No matter how you decide to create your list, make sure you intersperse unrelated, pleasurable books into your list. Aside from the psychological relief and burnout prevention you get from it, this also allows your brain to wander a bit and make potentially unique high-level connections to what you’re learning. Intellectual wandering can also provide unexpected inspiration.


### Experience Funnel

This part is fairly straightforward, but of serious importance. While learning, you need to ensure that you’re also getting as much first-hand experience with the subject as you can get. Of paramount importance is to practice somehow every day. As we discussed in the discipline section, you’ll need to start small and work up from there. A good starting point is around 30 minutes per day. If you can do 30 minutes per day for even a month, you’ll find that upping the time and difficulty of your practice routine will become easier and easier as time goes on.

This funnel can be tracked via a calendar, either digital or paper, and a simple word processor. I prefer Google Calendar and Google Docs for their excellent features and top-notch mobile integration, but use whatever works for you.

### Projects

Projects should become part of your experience funnel as well. As with everything else in your Factory, you should start small and work your way up. These are much harder to plan, as you can’t necessarily know ahead of time what you’ll be capable of doing. Instead, commit to creating lots of small projects as you progress. Each one should reflect your current skill level, as well as incorporating components of that are just beyond what you think you can do. 

Don’t start out trying to build something massive and complex. Instead, consider each piece of a more complex project and try to create each component on its own. Every subject has a project like this that can be broken down into many different pieces, so keep your eyes open and actively think about what you can build while you’re learning.