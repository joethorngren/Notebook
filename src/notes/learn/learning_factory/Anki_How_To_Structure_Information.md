## Rules for Structuring Anki Cards

### Rule #1: Use Cards as Supplements, Not Sources

- Don't download/use other people's decks

### Rule #2: Break Information Up Into Small Chunks

Overly Complex Card: 

**Front:** What countries make up the European Union? 
**Back:** Austria, Belgium, Bulgaria, Croatia, Cyprus, Czech Republic, Denmark, Estonia, Finland, France, Germany, Greece, …

Easier Way to Break the List Down:

**Front:** What European Union countries have names starting with "G"? 
**Back:** Germany and Greece

### Rule #3: Make Your Cards Vivid

- Place as many bits of audio and visual stimulation on your cards as you can. 
- Whenever possible, include images and sounds on your cards that will stick out to you.
- Using distinct images and sounds also avoids the issue of [interference](https://en.wikipedia.org/wiki/Interference_theory).

### Rule #4: Put Everything In One Deck

- What about sub-decks within one mega-deck?

### Rule #5: Stay Organized

- **__Always__** use tags (lots of them)
- For media, no more copy paste (automatically generates a long, random filename for each file and it also looks hideous to have your files placed directly onto your card’s template. 

### Rule #6: Keep Track of References

- Add two reference fields to note types: Online + Offline
- Online References: Hyperlinks
- Offline References: For example, if I were reading The Art of War by Sun Tzu and came across something I wanted to put into a flash card, I’d place the following into the offline reference field: “The Art of War (Sun Tzu)”.

### Rule #7: Focus on Subjects, Not Factoids

### Rule #8: Don't Make Cards For Things You Already Know
