## When to Create Fields

**Start Simple, Add as Needed**

> I started out, like most novices, eager to absorb every last bit of information about the people I created notes about. There were fields for date of birth, date of death, religion, political party, net worth and a variety of other trivial points. Looking back on it now, I can’t help but laugh at how needlessly complex they were.

One rule of thumb I use is fairly simple: if I find myself constantly cramming information about one aspect of an object into a note, I’ll make a field.

> For example, on my “People” notes, I kept adding people’s nicknames like this: Grace Hopper (“Amazing Grace”). Rather than add that extra clutter to the field, it’s much better to just create a new field. In this example case, I just added a “Nickname” field. This chopped the information into smaller, more easily digestible chunks, and cleaned up the information in my “Name” fields.

Diagram: 

![When to Create Fields](/res/learn/when_to_create_fields.png))

#### Tip: Don't Style Fields, Style Their Outputs

## Adding Images and Sounds


### Images

I found a suitably good picture of Grace, resized it (originally 2400x3000!) with GIMP (a free image editing program) and renamed it to “_Grace Hopper.jpg”. 
> **The underscore is crucial here because Anki will delete images that aren’t present in cards, and the way we use them in our templates will make Anki think that they are not in use.**

![Adding Images Example](/res/learn/adding_images_example.png))

The styling code will remain constant for every card type within the template, but you will need to go to each card and add the `<img src=”_{{Name}}.jpg”><br />` part if you want them to have images. 

In this example, you’ll need to ensure all of your images are named `_<filename>.jpg`, otherwise they won’t show up. Some people prefer other image formats (such as .png), so if you’re one of those people, by all means, change the extension to something else.

### Sounds

Sounds are added very similarly, but Anki has a specific syntax you need to use in order to utilize them. First, you need to either create the sounds or find them somewhere on the internet.

If you’re learning a foreign language with flash cards, this is especially important. Don’t just say the words - go to a site like [Forvo](http://forvo.com/) and download an MP3 of a native speaker pronouncing it. Neglecting to do this means means you run the risk of etching an incorrect pronunciation into your memory, making it very difficult (but not impossible) to fix later on.

Either way, once you have the file, add an underscore to its name and give it the name of a field. In this case, we want the name of the person, so the file should be named “_Grace Hopper.mp3”. Once we’ve done that, we go back to our templates and add the following code: 

`[sound:_{{Name}}.mp3]` 

This tells Anki to look for a MP3 file that corresponds to whatever is in the name field on your note. For this particular type of note, the rule of thumb is to place the sound file on whichever side has the person’s name and picture. So sounds should be inserted into the front templates for “Occupation” and “Country of Birth” and the back template for “Notes” (since the identifier card has the person’s name on the back, not the front).

### References

#### Online

Click inside the “Back Template” window on the bottom left of the screen and enter this bit of code: 

`<hr> {{#Reference}}<a href=”{{Reference}}”>Reference</a>{{/Reference}}`

Code above uses conditional replacement:

>When you enclose a field within a conditional replacement, you’re simply telling the program “show this field if it is (or is not) empty.” These ensure that only information you enter ends up on a card.
 
 This specific type of conditional replacement tells the card to hide the field unless it has something in it. In this case, if the “Reference” field is totally empty, you won’t see anything in the resulting card. However, if you put even a single character in “Reference” it will display the output.

#### Offline
 `{{#Reference (Offline)}}<br /><br /> <em>{{Reference (Offline)}}</em>{{/Reference (Offline)}} {{#Wikipedia}}`
 
#### Wikipedia

Add one more field called “Wikipedia.”

`{{#Wikipedia}} <br /><br /> <a href=” https://en.wikipedia.org/wiki/{{Wikipedia }} >Wikipedia</a> {{/Wikipedia}}`

The only difference between this code and the snippet you used for “Reference” is that there is a piece of code that points you to a Wikipedia web address. When you fill out the “Wikipedia” field, you only have to enter the name of the Wikipedia page and not the entire web address. For example, you can now enter “Neuron” into that field instead of dropping “ https://en.wikipedia.org/wiki/Neuron ” into the “Reference” field.

### Putting it all Together: 

`<hr> {{#Reference}}<a href=”{{Reference}}”>Reference</a>{{/Reference}} {{#Reference (Offline)}}<br /><br /> <em>{{Reference (Offline)}}</em>{{/Reference (Offline)}} {{#Wikipedia}} <br /><br /> <a href=” https://en.wikipedia.org/wiki/{{Wikipedia}}>Wikipedia</a> {{/Wikipedia}}`

![References Example](/res/learn/references_example.png))