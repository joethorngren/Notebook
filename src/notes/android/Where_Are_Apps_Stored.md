Where an app is stored very much depends on several criteria:

System apps / pre-installed-bloatware-apps are stored in /system/app with privileged apps in /system/priv-app (which are mounted read-only to prevent any changes)
normal apps in internal memory go to /data/app
some apps (encrypted on internal storage?) go to /data/app-private
Apps stored on external memory go to an encrypted container in /mnt/sdcard/.android_secure. As at runtime Android needs them to be decrypted, it will decrypt them and store a decrypted copy on tmpfs (so it's gone on a reboot) in /mnt/asec
(you cannot simply look into /mnt/sdcard/.android_secure directly from the device; but if you use a card reader and attach the card to your PC, you will see the files there have the extension .asec instead of .apk -- from which you will get the connection to the name /mnt/asec).
the apps data are stored below /data/data/<package_name> (internal storage) or on external storage, if the developer sticks to the rules, below

==================================================================================

package com.getnotion.android;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.security.auth.x500.X500Principal;

import io.realm.RealmConfiguration;

public class RealmEncryptionUtils {

    private static final String KEYSTORE_PROVIDER_NAME = "AndroidKeyStore";
    private static final String REALM_ENCRYPTION_PREF_KEY = "REALM_ENCRYPTION_PREF_KEY";
    private static final String REALM_ENCRYPTION_KEYSTORE_ALIAS = "REALM_ENCRYPTION_KEYSTORE_ALIAS";
    private static final String MARSHMALLOW_TRANSFORMATION = KeyProperties.KEY_ALGORITHM_AES
            + "/" + KeyProperties.BLOCK_MODE_CBC
            + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7;
    private static final ByteOrder ORDER_FOR_ENCRYPTED_DATA = ByteOrder.BIG_ENDIAN;

    private static RealmEncryptionUtils instance = null;

    private RealmEncryptionUtils() {
    }

    public static RealmEncryptionUtils getInstance() {
        if(instance == null) {
            instance = new RealmEncryptionUtils();
        }
        return instance;
    }

    public byte[] initRealmEncryption(Context context) {
        String ivAndEncryptedRealmKey = getExistingKeyInPreferences(context);
        if (ivAndEncryptedRealmKey == null) {
            Log.d("***DEBUG***", "initRealmEncryption: no existing key in SharedPreferences...");
            Log.d("***DEBUG***", "initRealmEncryption: generating realm secret key...");
            SecureRandom secureRandom = new SecureRandom();
            final byte[] keyForRealm = new byte[RealmConfiguration.KEY_LENGTH];
            secureRandom.nextBytes(keyForRealm);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                generateKeyInKeystoreMarshmallow();
            } else {
                generateKeyInKeystoreJellyBeanMR2(context);
            }

            byte[] encryptedRealmKey = encryptKeyForRealm(keyForRealm);
            saveEncryptedKeyToPreferences(encryptedRealmKey, context);

            ivAndEncryptedRealmKey = Base64.encodeToString(encryptedRealmKey, Base64.NO_PADDING);


        }
        return decryptIvAndEncryptedRealmKey(ivAndEncryptedRealmKey);
    }

    private String getExistingKeyInPreferences(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(REALM_ENCRYPTION_PREF_KEY, null);
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void generateKeyInKeystoreMarshmallow() {
        final KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES,
                    KEYSTORE_PROVIDER_NAME);
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException(e);
        }

        final KeyGenParameterSpec keySpec = new KeyGenParameterSpec.Builder(
                REALM_ENCRYPTION_KEYSTORE_ALIAS,
                KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                .build();
        try {
            keyGenerator.init(keySpec);
        } catch (InvalidAlgorithmParameterException e) {
            throw new RuntimeException(e);
        }
        keyGenerator.generateKey();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void generateKeyInKeystoreJellyBeanMR2(Context context) {

        try {
            KeyPairGeneratorSpec keyPairSpec = new KeyPairGeneratorSpec.Builder(context)
                    .setAlias(REALM_ENCRYPTION_KEYSTORE_ALIAS)
                    .setSubject(new X500Principal("CN=Sample Name, O=Android Authority"))
                    .setSerialNumber(BigInteger.ONE)
                    .build();
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
            generator.initialize(keyPairSpec);
            KeyPair keyPair = generator.generateKeyPair();
        } catch (InvalidAlgorithmParameterException | NoSuchProviderException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private byte[] encryptKeyForRealm(byte[] keyForRealm) {
        try {
            final KeyStore ks;
            ks = KeyStore.getInstance(KEYSTORE_PROVIDER_NAME);
            ks.load(null);
            final Cipher cipher = Cipher.getInstance(MARSHMALLOW_TRANSFORMATION);

            final byte[] iv;
            final byte[] encryptedKeyForRealm;

            final SecretKey key = (SecretKey) ks.getKey(REALM_ENCRYPTION_KEYSTORE_ALIAS, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);

            encryptedKeyForRealm = cipher.doFinal(keyForRealm);
            iv = cipher.getIV();
            final byte[] ivAndEncryptedKey = new byte[Integer.SIZE + iv.length + encryptedKeyForRealm.length];

            final ByteBuffer buffer = ByteBuffer.wrap(ivAndEncryptedKey);
            buffer.order(ORDER_FOR_ENCRYPTED_DATA);
            buffer.putInt(iv.length);
            buffer.put(iv);
            buffer.put(encryptedKeyForRealm);

            return ivAndEncryptedKey;
        } catch (InvalidKeyException | UnrecoverableKeyException | NoSuchAlgorithmException
                | KeyStoreException | BadPaddingException | IllegalBlockSizeException | CertificateException | NoSuchPaddingException | IOException e) {
            throw new RuntimeException("key for encryption is invalid", e);
        }
    }

    private byte[] decryptIvAndEncryptedRealmKey(String ivAndEncryptedRealmKey) {
        try {
            byte[] decryptedIvAndKey = Base64.decode(ivAndEncryptedRealmKey, Base64.DEFAULT);

            final KeyStore keyStore = KeyStore.getInstance(KEYSTORE_PROVIDER_NAME);
            keyStore.load(null);
            final Cipher cipher = Cipher.getInstance(MARSHMALLOW_TRANSFORMATION);

            final ByteBuffer buffer = ByteBuffer.wrap(decryptedIvAndKey);
            buffer.order(ORDER_FOR_ENCRYPTED_DATA);

            final int ivLength = buffer.getInt();
            final byte[] iv = new byte[ivLength];
            final byte[] encryptedKey = new byte[decryptedIvAndKey.length - Integer.SIZE - ivLength];

            buffer.get(iv);
            buffer.get(encryptedKey);
            final SecretKey key = (SecretKey) keyStore.getKey(REALM_ENCRYPTION_KEYSTORE_ALIAS, null);
            final IvParameterSpec ivSpec = new IvParameterSpec(iv);
            cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);

            return cipher.doFinal(encryptedKey);
        } catch (InvalidKeyException e) {
            throw new RuntimeException("key is invalid.");
        } catch (UnrecoverableKeyException | NoSuchAlgorithmException | BadPaddingException
                | KeyStoreException | IllegalBlockSizeException | InvalidAlgorithmParameterException e) {
            throw new RuntimeException(e);
        } catch (CertificateException | NoSuchPaddingException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void saveEncryptedKeyToPreferences(byte[] ivAndEncryptedKey, Context context) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(REALM_ENCRYPTION_PREF_KEY, Base64.encodeToString(ivAndEncryptedKey, Base64.DEFAULT));
        editor.apply();
    }
}
