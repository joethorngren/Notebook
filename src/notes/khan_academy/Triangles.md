Congruent Triangles
-------------------

SSS --- Side Side Side
SAS --- Side Angle Side
ASA --- Angle Side Angle
AAS --- Angle Angle Side

All four imply congruency

Classifying Triangles I
-----------------------

**Sides**:
 
- Scalene: No equal Sides
- Isosceles: At Least Two Equal Sides
- Equilateral: Three Equal Sides
- All Equilateral Triangles are Isosceles Triangles
- Some Isosceles Triangles are Equilateral Triangles
	
**Angles**:

- Acute: All angles are less than 90 degrees
- Right: Has one angle that is exactly 90 degrees
- Obtuse: Has one angle that is larger than 90 degrees