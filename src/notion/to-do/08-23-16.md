### To Do:
    
- Before I Leave: 
    - [x] Setup + Test Kevo
    - [x] Get Desk Make + Model
    - [x] Contact Brett about office keys + background survey
    - [x] Check iPod Touch Version
    - [x] Get iPhone 4S to take with me 
    - [x] Respond to Bryan via Slack about "One thing you wish you knew that would make you awesome at your job...something something"
    ~~- [ ] Email Brett + Ryan about official start date September 12th~~
    ~~- [ ] Alpha or Staging Kit to take with me?~~
    
- Soon
    - [ ] Figure out Notion Alpha "Item Not Found" ---> troubleshoot  
    - [ ] Take a tour of the Notion Google Drive
    - [ ] Figure Out USB-C Connected Monitor Sleep/Wake Issues
    - [ ] Add Sean's Blog to Feedly  
    - [ ] Sync Android Studio + IntelliJ Plugins b/w ZenBook + LightHouse 
            - ADB IDEA
            - Kaushik Fragmented... Tips and Tricks... Something something. 
            - Others...? 
    - [ ] Android Studio (just Canary maybe?) double's font size after restart...?
    - [ ] Setup Git Repo for this Project + Push
    - [ ] Sync IntelliJ/Android Studio Shortcuts from LightHouse to Notion Setup
    - [ ] Dual Boot Linux
    - [ ] Convert Links from Above to Chrome Shortcuts
    - [ ] Review Onboarding Slides-- Particularly Mission Statement 
    - [ ] Setup OnePassword---Migrate LastPass Shizz---Kill Lastpass??? 
    - [ ] Setup Calendar Reminders for Reoccurring Meetings
    - [ ] Setup CLE Environment (REALLY SOON!!!)
    - [ ] Sync Display Fusion Settings from LightHouse to Notion Setup 
    - [ ] Display Fusion showing monitors using Intel HD Graphics instead of GTX 960?
    - [ ] Download/Install Launchy
            - Window is super small
            - Configure Skins
            - Configure Catalog
    - [ ] Camtasia Studio 
    - [ ] Setup Chrome Persona's
    - [ ] Babun
    - [ ] Fabric Push Notifications to JT Phone
    - [ ] Upgrade to Windows 10 Pro--Cost? 
    - [ ] Check for BIOS updates on ZenBook + LightHouse
    - [ ] Complete joe@getnotion Gmail setup
    - [ ] Figure Out Taskbar Issues
    - [ ] After ALT + Tab, remove stupid chooser dialog
    - [ ] Display Fusion Script-- Popup Menu for common stuff (Monitor Configuration + Settings + Wallpaper + etc.) 
    - [ ] Follow Notion on Twitter 
    - [ ] Catalog List of ToDo's inside of Android App
    - [ ] Postico -PostGRES client- ---> Use Database Connector inside AS/IntelliJ
    - [x] Download/Install SnagIt
          - [ ] Configure SnagIt Hotkeys  
    - [x] Change Windows Screen Orientation Hotkeys (NOW) 
    - [x] Set Chrome as Default Browser 
    - [x] Download/Install Clipboard Fusion
    - [x] Download/Install Synergy 
    - [x] Remove OneDrive
    - [x] Add Slack to Startup
    - [x] Download VLC + Set Default
    
- Soonish
    - [ ] Flashcards/ImageQa Circuit Board Overview (Currently Saved on ZenBook in Pictures) 
    - [ ] Recreate Notion GitHub account 
    - [ ] Comb Slack for Acronyms + Other Juicy Nuggets

    
### Need: 
- [ ] Monitor Stands/Raisers 
- [ ] Docking Station
- [ ] USB Hub-- Still necessary after docking station? 
- [ ] Chair 
- [ ] Plant
- [ ] Bookshelf that isn't obtrusive
- [ ] Cord Chaos Under Control
- [ ] Second Charger for Asus Zenbook 
- [ ] ZenBook Expandable Memory/Storage ---> External HDD if not?