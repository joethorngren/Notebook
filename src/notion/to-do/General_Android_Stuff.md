### General App To Do: ***Needs to be Prioritized***

- [ ] Espresso
    - To have full Espresso coverage, we need DI
    - Dagger 2 ---> Eeeks. 
    - ToothPick???
- [ ] JUnit/Robolectric ---> Hopefully mostly JUnit? 
- [ ] Jenkins
    - Track Metrics
        - [ ] App Startup
        - [ ] Dex Count
- [ ] RetroLambda---> Should be an easy win, flip the switch + profit 
- [ ] DataBinding (If it's easy to kill Butterknife, then let's do it)
- [ ] Upgrade to RetroFit 2 + OkHttp3
- [ ] Timber
- [ ] No Splash Screen on App Launch 


### Joe To Do:

- [ ] Map out the NotionApi 
- [ ] Include Gradle Plugin for Dex Count so we can keep track of where it's at
- [ ] Figure out how to handle permissions for Espresso Tests
    - UiAutomator (Gross) 
- [ ] Gradle Plugin for Dex Count 
// Unrelated: Figure out Links so I can link to lines #18 + #23*** // 
- [ ] Checkstyle or the thing that Garrett was talking about 
- [ ] Checkout Google GeoFence APIs

### Dream Big

- AirBnB auto-connect to secured WiFi ---> Woah. 
- Getcha Rx On 