Plug in a Bridge: 

JT Notion Accounts:

Alpha: joe@getnotion.com
Staging: TBD
Prod: joethorngren@gmail.com 

### Hardware Shizz

[Document on Confluence describing different flashing states](https://notion.atlassian.net/wiki/display/NOT/Bridge)

- Flashing Blue-- Slow: Access Point Mode
    - Broadcasting Notion Bridge-SSID 
    - Ready to be configured
    - Doesn't have WIFI credentials, might or might not have an account
- Flashing Green-- Connected
- Flashing Blue-- Fast
    - Incorrect Credentials
    - After Bridge is plugged in, it's only an access point for 30 minutes
- Flashing Red: Can't connect to Notion Backend
- Flashing Red/Blue intermittently, it's updating 

Bridge is nearly a proxy, cannot read sensor messages
ID of bridge/sensor is how we look up data on the backend

### App Specific Stuff

Longpress on logout will give you the option to delete your account 

Setup Screen is called: InitialLoginActivity** 
Email Field + Password Field == Collapsible fields ---> Validation Listeners*
    * Good spot for ObjectTransitions 

Create System Screen === Add New Bridge Activity 
    - Okay, plugged in ---> Turns on WiFi 

WAC chip on the bridge allows iOS app to use native system WiFI connection screens + pass off the credentials

After Scanning Sensor QR Code, Select Location ---> Location Selection Code 

For Screens that have a native Android UI backbutton on the toolbar ---> They're old, haven't been touched in awhile 

Sensor Temperature Task: iOS shows graph/chart ---> No dice for Android 
Smoke Alarm Task Details: Android shows 1.0 --> alarm detected || blank ---> no alarm

Android---> Edit System Location: Cannot drag/move pin 

Android: Secondary Contacts 

Android Badging across the entire app

### Oh really?? 

Be aware that when you run in to issues during Alpha + Staging, there is a possibility that it might be an Android bug, not a Joe/Garrett bug

Change_WiFi_State is a "soft" AKA not dangerous permission

Reset Sensor: 1) Take battery out + wait a minute OR
              2) Discharge the capacitor   

Only allow 1 sensor per location

Cannot have white status bars in Android ---> Android Icons are white 

Sensor hello's every 30 seconds
 
Select your room + select your tasks ---> See install screen, click install now ---> Callibrating Sensor **Biggest Pain Point in the App. Blocking. Takes up to a minute to calibrate. 

### To Do:

- Take a second look at setup/login validation
- "Pseudo" Toolbar ---> Janky/Not Material/Colors match status bar

- Create Account: Name Field Placeholder text (should be greyed out)
- Look up Collapsible Field

- When not connected to WiFi network with WiFi turned on, shows dialog of available WiFi networks, needs styling 
- Entering wifi password runs over
- Add Sensors---> Knock Twice to Wake Up---> Ask for Permissions (No dialog explaining why camera, pretty self-evident, but still)
- After connecting to Bridge, status messages at the bottom: Needs new background color + font color 
- After selecting location ---> Toast message "Adding Location" ---> Need to switch that to a snackbar 

- "Teach Screens" 

### Differences

- Temperature Task Details 
    - Graph
- Secondary Contacts
- In-app feedback:
    - Basic form ---> Click send, pulls up email and attaches log files
    - Examples from other apps:
        - Zendesk
        - Hangouts
        - Others?
- Informational Badges
    - Sensor is disconnected
    - Verify emergency numbers
    - Teal  === Informational
    - Red === Errors
- Notification Shortcuts: iOS ---> Share with a friend/Disable notification inside app
    - iOS: If you interact with the push notification, it takes you inside the app and slides the relevant dialog up
    - Android: Bridge/Sensor ---> takes you to the homescreen and slides relevant menu up (See SlideupNotificationDialog.java)

------------------------------------------------

From email to USAA:

What are the main differences between Android and Apple app functionality?

- Temperature Task Details: iOS has Graph
- Secondary Contacts (Share alerts)
- In-app Feedback
- Informational Badges: iOS has badges on their icons that indicate informational (i.e., verify emergency contact info) + errors (i.e., sensor is disconnected) 
- Notification Shortcuts: iOS can share alerts/notifications with a friend as well as change notification preferences from the push notifications
    
Any plans to be able to send support feedback directly through Android app?

- Improved app feedback similar to iOS will happen in the next update alongside Security update at the end of September

When is the next Android app update scheduled?

- Wednesday, September 28th 2016
