Agenda:

- Concerns
    - Mentor
    - Push to Feature Parody--- Best move? 
        - Quote from presentation:
        > Why do we need it? Why do we need to do dependency injection, dependency inversion, or any of this? To me, good code is testable code. By that I mean it’s not code that has tests, but it’s at the very least code that you can write tests for. The reason that you want to write tests is to have less developer anxiety.
          
          > I started at the Times about a year ago, and I was really happy that we had about 1,000 unit tests at that point, which made it dramatically easier for me to commit new code without breaking something in production. Similarly, now that we have interns starting for the summer, they also are going to be able to benefit from us having more tests.
          
          > For us to have tests, what we need to do is structure our code base in a manner where it is testable. Not all code is testable, only code that is organized in a certain manner can be tested.
          
          
          From Clean Code: 
          
          ![Clean Code](/res/clean_code.png)
          
    - Garrett spending more time in Android Land
    - Brett introduced me to Liz, "Here's our lead Android Developer"
    
    
    