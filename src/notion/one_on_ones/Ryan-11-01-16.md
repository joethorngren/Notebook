Ryan 1v1 Agenda:

- How's it going? Feedback? Group meetings, 1 on 1s?
- Parkifi-- Who's in the know? 
- DT Prescriptions
- Life After Security Release:
    - Release Retrospective  
    - Priorities: 
        - CI    
        - One "Source of Truth"
            - iOS v. Android Feature Gap: Re-visit, consolidate lists---> Make actual stories  > Start belting these out 
            - Consolidate Confluence/JIRA existing bug lists---> Make actual stories           
            - Streamline workflow with Rossi + Julian
            - Integrations with Google Play Developer Console (Create new cards automagically)
    > One Master Trello Board               

- Life Until Security Release
    - Grind it out
   
- I'm wanting to put a brown bag on... Two weeks? Maybe three?    
- Docking Station: https://www.amazon.com/Belkin-Thunderbolt-Transfer-Compatible-F4U085tt/dp/B00NAWCU7G/ref=as_li_ss_tl?ie=UTF8&qid=1451411965&sr=8-1-spell&keywords=Belkin%2BThunderbolt%2B2%2BExpressDock&linkCode=sl1&tag=omnideals-20&linkId=64252136f45184013e0dd24b2cd45b08&th=1
