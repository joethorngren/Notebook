ReportID	Sensor Hardware Revision	Data Format
0	
1		
2		
3		
4   	
5		
6		
7		
8		
9			
0x0A	
0x0B	
0x0C	
0x15	
0x17	
0x73	
0x74	
0xD0	
0xDF	
0xE2	
0xE3	
0xE4	
0xED	
0xEE	
0xEF	
0xF0	
0xF1	
0xF2	
0xF3	
0xF4	
0xF7	
0xF8	
0xF9	
0xFA	
0xFB	
0xFC	
0xFD	
0xFE	
0xFF	

 Battery voltage may be obtained by the formula:Vbat (volts) = value *3.57 / 2048
 1st is from the IR (proximity) detector the 2ndis from the ambient light sensor. Range is 0 to 0x0FFF higher values indicate lower light levels. Unitless.
 Indicates the change in reading when IR LED is applied. 0 or less = no reflection. Higher numbers indicate increasing reflection (shorter range to reflective object).
 unit-less value indicating the maximum sound peak.
 Temperature = (value / 2) + 23 deg C.
 No data presence of message of this type indicates conduction between water detector probes.
 X Y and Z relative position.Note: exceeding maximum range will cause position to “roll over”. i.e. moving + 10 from 32765 will result in -32761.
 The data contains the 10 dominant frequencies and relative signal levels of a sound detected by the sensor. Each data group contains 2 bytes which are a 16 bit value indicating the frequency followed by 1 byte which is the signal level.Frequency is represented as 16 * value. That is a value of 62 represents 992 Hz (16 * 62). As of firmware 3.1.0 the useful range reported is approximately 17 to 497 (280 Hz to 7960) Hz although 0 to 511 is the firmware limit.Signal level is in dB 0 is full scale range is 0 to -127; although -80 is a approximately the limit of the detection algorithm (as of 3.1.0).
 No data presence of message of this type indicates low resistance short (conductive object) between water detector probes.
 Data structure describing motion 64 bytes in total containing the following (in order):quaternion q_start (128 bits = 16 bytes)quaternion q_finish (128 bit = 16 bytes)3D-Vector peak_accel (96 bits = 12 bytes)3D-Vector distance (96 bit = 12 bytes)integer duration (32 bits = 4 bytes)flags (32 bits = 4 bytes)Where a quaternation is defined as:floating point scalar (32 bits)and a 3D-VectorA 3D-Vector is simply 3 32 bit floating values for XYand Z direction.All fields sent BIG ENDIAN (MSB first)Duration is in milliseconds flags are defined as follows: (flags are bit masks and multiple flags may be set it some cases).0x00000001 = FLAG_ORIENT_DIFFERENTSet when orientation (gravity vector) changes but no accelration wake event occurred. Only orientation values are valid.0x00000002 = FLAG_TEMPERATURE_DELTASet when gyro temperature change detected.0x00000004 = FLAG_GYRO_BIAS_CALCULATEDSet when the GYRO bias is re-calculated0x00000008 = FLAG_MOTION_DETECTEDSet when motion is detected – results include initial to end angle and acceleration accumulator data.0x00000020 = FLAG_MOTION_CONTINUOUSSet when motion stimulation does not stop for maximum allowed time – motion detection disabled until stop is detected during periodic sample data returned is for end of data recording time (as indicated by duration)0x00000080 = FLAG_MOTION_MANUALSet when motion event was started bu a manual command not the accelerometer.
 Results of magnetometer or magnetometer after motion reading. The first 64 bytes are the same as described for motion (message ID 0x09) above.The remaining 8 bytes are 3 16 bit sign extended integer values for the magnetic field in the X Y and Z axises in order. Units are 1/16thμT (microteslas) and 1 16 bit unsigned value which is the resistance reading of the hall resistance which may be used to apply temperature corrections. See BMX055 data-sheet and driver for correction application.Notes: The vector is relative to the sensor in the following fission:X axis: line across the face parallel to the word “notion” with north to the left of the sensor when “notion” is visible for a + value.Y axis: a line across the face of the sensor perpendicular to the word “notion” with north above the word for a + value.Z axis: a line straight out the front face of the sensor with north behind the sensor for a + value.Most door installations will have +y to the top of the door.This is NOT rotated for gravity as motion vectors are – this is because rotation around gravity accumulates an error for which this reading is used to compensate – rotating would nullify the value of the data.Noise in reading will be approximately 0.6 μT earth's nominal magnetic field is 25 to 65 μT depending on location on the earth. Zero-B offset built into the sensor is +/- 40uT range is at least +/- 1.2T X and Y and 2.0 T Z rail/saturation is not detected.The additional flag field 0x00000010 indicates magnetic field data is valid for end of motion event.Additional flag field:0x00000010 = FLAG_MAG_DATAResults include magnetometer data.
 Hinged motion report. Byte 0: Extended flags0x01 = EXT_FLAG_VERTICALdata is for a vertical hinge when setByte 1: 8 bit value field – Flags.0x01 = FLAG_ORIENT_DIFFERENTSet when orientation (gravity vector) changes but no accelration wake event occurred.0x02 = FLAG_TEMPERATURE_DELTASet when gyro temperature change detected causing gyro bias recalc0x04 = FLAG_GYRO_BIAS_CALCULATEDSet when the GYRO bias is re-calculated0x08 = FLAG_MOTION_DETECTEDSet when motion is detected .0x10 = FLAG_MAG_DATAResults include magnetometer data.0x20 = FLAG_MOTION_CONTINUOUSSet when motion stimulation does not stop for maximum allowed time – motion detection disabled until stop is detected during periodic sample data returned is for end of data recording time (as indicated by duration)0x80 = FLAG_MOTION_MANUALSet when motion event was started bu a manual command not the accelerometer.Bytes 2-3: "Result" - 16 bit 2's compliment integer degreesVertical Hinge: angle travel during event (+ = CW)Horizontal Hinge: absolute ending angle relative to gravityBytes 4-5: "Max Up" 16 bit 2's compliment integer degreesMaximum travel CW (V) or up (H) during event.Bytes 6-7: "Max Down"16 bit 2's compliment integer degreesMaximum travel CCW (V) or down (H) during event.Bytes 8-9: 16 bit 2's compliment integerVertical Hinge: "Maximum angle rate" (deg/sec) during event.Horizontal Hinge: "X ending gravitational" value (cm/sec or m/s*100)Bytes 10-11: 16 bit 2's compliment integerVertical Hinge: "Initial Angle rate" (deg/sec) at start of event.Horizontal Hinge: "Y ending gravitational" value (cm/sec or m/s*100)Bytes 12-13: 16 bit 2's compliment integerVertical Hinge: "Maximum acceleration" during event (cm/s2or m/s2*100)Horizontal Hinge: "Z ending gravitational" value (cm/sec or m/s*100)Byte 14-15: 16 bit 2's compliment integerEnding magnetometer X value (magnetometer raw units)Byte 16-17: 16 bit 2's compliment integerEnding magnetometer Y value (magnetometer raw units)Byte 18-19: 16 bit 2's compliment integerEnding magnetometer Z value (magnetometer raw units)Byte 20-21: 16 bit integerDuration of event in ms
 Sliding motion report. 18 bytes of data:4 byte float = distance traveled in meters from start to end position.4 byte float = maximum distance from start of motion2 byte (16 bit signed) X axis magnetic field (as described above) at end of travel2 byte (16 bit signed) Y axis magnetic field (as described above) at end of travel2 byte (16 bit signed) Y axis magnetic field (as described above) at end2 byte (16 bit unsigned) duration of motion in ms2 byte flags (see below)Flags field is logical or of:0x0008 motion measured (distance fields are valid)0x0010 Magnetic field measured (if only this bit is set field has changed without motion measurement)0x0020 Continuous motion detected. Set when motion stimulation does not stop for maximum allowed time – motion detection disabled until stop is detected during periodic sample data returned is for end of data recording time (as indicated by duration)0x0040 Hard stop detected (impulse acceleration peak indicates hitting a hard stopping position such as jam or stop)0x0080 Manual Command initiated – set when event was started by command not accleration.NOTE: If motion is in impossible directions (installation error sensor is removed from surface or surface is removed from sliding guides sensor will return to NORMAL/DEFAULT motion mode and report the mode change event with a 0x73:0x00 message). Z axis (through top of the sensor) must be PARALLEL to the ground (this will be the normal install).
 Water probe normal. No data – indicates the end of a short or leak event.
 Results report of natural frequency ping. format is the same as a sound sample report.
 Debug message – 32 bit payload meaning varies with firmware.
                                            Indicates selected motion model:0x00 = NORMAL / DEFAULT0x02 = Hinged vertical hinge0x03 = Hinged horizontal hinge0x04 = Sliding vertical movement0x05 = Sliding horizontal movement
16 byte encrypted message	                Payload (indicated number of bytes) contains AES CBC encrypted message using the Notion global initialization vector and the bridge's or sensor's unique 256 bit key created at sensor production. When decrypted the result will be a payload with the following format:1 control byte = (M << 4) + N where M and N are 0 to 15.N bytes of random data to be ignored.X bytes of message data. X = encrypted length – (N + M + 2) The message data will be formatted as described in this document for message types except encrypted messages.1 byte of checksum (checksum + sum(message) = 0xFF)M bytes of random data to be ignored.
Message IdentityThis message can be safely ignored by the back end provided it continues processing and messages after this message ID and it’s 2 data bytes.2 data bytes. Byte 0 = Option code; Byte 1 = message ID.Option code:0x00 = normal message – the message ID should not match the previous message ID if it does this message is a duplicate of the previous message and can be ignored. (Bridges with up to date firmware should delete duplicates.)0x01 = message ID reset – Instructs the bridge that this message should be forwarded and reset its message ID to this messages ID regardless of past IDs.0x02 = unnumbered message – this message does not have an ID its ID should be ignored message should always be forwarded to the BE even if duplicate is indicated this messages ID should not be checked for duplicate detection in the next normal message. That is – handle as you would any message payload without a message identity.0x03 = do not forward – this message should not be forwarded by the bridge (the BE will only get this option code if the bridge does not support duplicate detection – this option was created for testing this feature)0x04 – 0xFF = reserved
Message only from a bridge – is a report of the wireless state of the bridge. Message payload is as follows:Byte 0: Currently selected WiFi channel numberByte 1: Currently selected WiFi channel signal level (singed dBm)Byte 2: Most powerful WiFi network found in scan (excluding current)Byte 3: Most powerful WiFi network found in scn signal level (dBm)Byte 4-9 – next 3 wifi channels in channel number / level order as above.Byte 10: 802.15.4 channel 11 energy level (see datasheet unsigned)Byte 11: 802.15.4 channel 12 energy levelByte 13 – 27: channels 13 – 28 energy levels
Message only from a bridge - 802.15.4 status. 3 byte payload:byte 0: integer indicating selected 802.15.4 channelbyte 1-2: current PAN ID
Buffer Overflow - messages dropped.  Message from bridge - indicates a bridge buffer overflow occurred (likely because of a back end connection issue).  Has a 2 byte payload (MSB first) containing an unsigned count of the number of messages dropped (a saturated value of 0xFFFF will be used if 65535 or more messages are dropped - may this never happen).
TX Power Indicator. 1 byte data = sensor transmit power
RESERVED (This Echo Request message may be generated by a sensor but the bridge will reply to the sensor directly without forwarding to the back end).
Echo Reply. This message is generated by a sensor in response to an Echo Request message. There are 2 bytes of payload:1stbyte = RSSI in dBm2ndbyte = LQI (Link Quality Indicator) accounts for channel noise range 0 (poor) to 255 (excellent).These bytes represent the level and quality of the ECHO REQUEST AS RECIEVED BY THE SENSOR and are not the quality of the reply itself.(This message will be from sensor devices only never from a bridge – although the sensor may send a ping request to a bridge the reply will be to the sensor radio neither the request nor reply will be transmitted to the back end.)
Sensor: 3 16 bit unsigned values. In order major version minor version build/patch version.Bridge: (30 bytes of data) 5 sets of 3 16 bit unsigned values. Each containing a major minor and build/patch version. The sets will be in this order:1. Ti 802.15.4 version.2. Wifi module application version.3. Wifi module wifi version.4. BLE module softdevice version.5. BLE module application version.Any values unavailable at the time of the incoming “hello” message will be reported as 0.0.0.
Bridge:7 byte configuration reportfirst byte0x00 = Alpha bridge0x01 = staging bridge0x02 = production bridge0x03 = private (test) server bridgebytes 2 - 7: 6 byte beacon payload.Sensor:16 bit value indicating sensors enabled. Each bit represents a function bit = 1 indicates function enabled.Bit 0 (LSB) = LightBit 1 = ProximityBit 2 = AccelerationBit 3 = Sound LevelBit 4 = Water probeBit 5 = TemperatureBit 6 = VBatBit 7 = Sound IdentificationBit 8 = Magnetometer EnabledBit 9-15 = Reserved for future use.
8 bit data indicating selected threshold for light threshold (range 0 to 16 decimal)
8 bit data indicating selected G-Force threshold
8 bit data indicating selected audio gain
No data – presence of this message type indicates an invalid instruction was given to the sensor.
No data – simple ping / keep alive sent to the back end (back end should reply with 0xF8 reply if no other data to send) This message will only be generated by a bridge (sensor ID = 0) and is not expected from a sensor.
8 bit data representing the relative 64 byte block. This message indicates an acknowledge of the block received.
8 bit data indicating the sector number (upper 8 bits of the 19 bit address) of the firmware file to be transferred. All addresses are therefor on 2K boundaries (matching on-die flash layout). Data request shall be assumed to be for 16K (sector) of data initiating at the address indicated by appending 7 bits of 0 to the value passed.
No data – presence indicates that the device is entering “box” mode for storage / shipping. This will be the last message from this sensor until it awakens when removed from packaging.
No data – presence indicates that the motion system has calibrated (sensor is in normal operating mode)
FROM SENSOR: RESERVEDFROM HUB (sensor ID = 0): Indicates a sensor has not reported to the hub long enough to be considered lost from this hub. This message has an 8 byte data field that contains the sensor ID that is being reported as lost.
Reserved (used between hub and sensors to identify notion compatible hubs).
Integer value sensor hardware revisionsensor revision 2 = First release sensor build (alpha/beta test)sensor revision 3 = rev 7.x PCBa (new accel change to vref/vbat)bridge revision 1 = Rev 3 PCBbridge revision 2 = Rev 5 PCB


0x10-0x13	3	Combination sound message containing 0 1 2 or 3 FFT reports (30 bytes each) plus sound duration (2 bytes) and reason code (1 byte).0x10 = 0 (zero) FFT reports0x11 = 1 FFT report0x12 = 2 FFT reports0x13 = 3 FFT reportsFormat for the FFT: each FFT report is the same as for message 0x07. Remaining three bytes are as follows:2 bytes unsigned representing the duration of the sound in ms1 byte unsigned representing a reason code. Current reason code values are:0x00 SOUND_MSG_END -- message sent because the sound ended0x01SOUND_MSG_LONG -- message sent because the sound ran long enough that a message was sent before it ended0x02 SOUND_MSG_MOTION -- message sent because motion was detected and sound processing had already started0x04 SOUND_MSG_PING --- message sent in reply to ping command.
0x70 `or0x72`	2-3	Single byte data – indicates an error on the sensor. Should be logged by the back end for diagnostic purposes. 0x70 and 0x72 shall be logged independently or such that they may be differentiated.Error Codes in appendix.
0x0D	Bridge	FROM HUB (sensor ID = 0): Indicates a sensor has initiated contact with this hub. This message has an 8 byte data field that contains the sensor ID that is associating with this bridge.
0xD1	32 byte encrypted message	
0xD2	48 byte encrypted message	
0xD3	64 byte encrypted message	
0xD4	80 byte encrypted message	
0xD5	96 byte encrypted message