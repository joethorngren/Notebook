Toothpick: A Fresh Approach to Dependency Injection (DI) on Android

Daniel Molinero Reguera and Stéphane Nicolas

Do you find RoboGuice simple but slow? And do you think Dagger 1 & 2 are fast but complex and bloated? Toothpick is the best of both worlds!

Toothpick is a scope-tree-based runtime but reflection-free implementation of JSR 330. It is pure Java, with a special focus on Android.

Toothpick is fast (even faster than Dagger 2 in some cases!) and is simpler to use, with less boilerplate code. Its syntax is very close to Guice. It supports named dependencies, lazy dependencies, providers, and has built-in support for custom scopes. As compared to Dagger 2, Toothpick provides more powerful testing support, allowing you to leverage DI to mock dependencies using either mockito or easymock.

Its explicit scope tree helps developers to build more robust apps. It provides more fine grained control to easily implement complex user flows that span multiple activities or state preservation across configuration changes (e.g. rotations), a common issue when implementing the MVP pattern.

Note: Some knowledge about Dependency Injection and Dependency Injection frameworks is recommended. Anyway, any user can find it useful as we will explain the features of the library and how it compares to the main frameworks nowadays.

Level: Advanced

http://www.andevcon.com/sf2016/sessions#ToothpickAFreshApproachtoDependencyInjectionDIonAndroid