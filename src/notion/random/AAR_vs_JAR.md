AARs vs Jars:

The main difference between a Jar and a AAR is that AARs include resources such as layouts, drawables etc. This makes it a lot easier to create self-contained visual components. For example if you have multiple apps that use the same login screen, with Jars you could share classes but not the layout, styles, etc., you still had to duplicate them. With AARs everything is bundled in one neat package.

https://stackoverflow.com/questions/23915619/android-archive-library-aar-vs-standard-jar
