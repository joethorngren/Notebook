

Bridge ---> TCP Socket ---> N-Serve


Bridge ---> N-Serve
Ongoing TCP Data Socket

N-Serve:

EventMachine (EM) spins off instances of connections.
---> Connection:
        - Protocol:
            - YAML descriptor that implements Bridge <---> Backend messaging
            - Specify Routing_Key (Designates which RabbitMq Exchange)
        - Broker (to RabbitMq)
---> ConnectionStatus (Singleton)
     Broker (handles connection to RabbitMq)
        |
        |
        |
        Connection to Redis:
        ---> Redis Caches:
             Bridge Connection State
             Sensor Connection State
             What sensors are connected to which bridge

As of release this week (10-19-16), difference N-Serve instances will be able to communicate to each other

Redis
    - "Redis is an open source (BSD licensed), in-memory data structure store, used as database, cache and message broker."
    - Memory Optimized Key/Value Store
    - Not Meant to be persisted
    - Temporary Cache
    - There are 2 different Redis instances throughout the backend:
        - N-Serve + API share 1
        - Apollo has his own-- If he dies, we're in trouble

Bridges usually start with F.......
Bridges that are shipped to Japan are going to start with E.......

Message format
Bridge Id    | Sensor Id     |  Length of Message     |  Timestamp (MS)    | Two Reserved Bytes that are not being used
|--------------------------------------------------------------------------|
                        Header


RabbitMq:
    - 1 instance throughout Notion BE
    - Four Components:
        - Connections
        - Channels
        - Exchanges:
            - Route Messages
            - Notion Exchanges:
                - Report Exchange: Hardware ---> AP
                    - State Queue
                    - Motion Queue
                    - Sound Queue
                    - Sensor State
                    - Sensor Motion
                    - Sensor Light
                    - Sensor Debug
                    - Bridge State
                    - Bridge Debug
                - Command Exchange: API ---> Hardware
                    - Each Bridge has its own queue---> Bridge ID == Routing Key
                    - Connection Status Sync Queue
                - Apollo Exchange:
                    -

Sneakers: Background Processing Gem (Implementation of RabbitMq)

API is started in one of four ways:
    - www (Mobile + Admin Portal)                                                                          Prod: Run 2
    - sidekiq - Pulls work off Redis Queue - Unordered General background processing (Redis)               Prod: Run 2
    - sneakers - Background Processing specific to RabbitMq                                                Prod: Run 2
    - clockwork (Chron Job replacement-- single instance)                                                  Prod: Run 1