09-08-2016

JIRA Issues:

- MOB 35 Create Android native secure method of Bridge provisioning as outlined in HAP SDK
- MOB 36 Enable encryption for local Realm databases
- MOB 37 Enable Certificate Pinning for Android

---------------------------------

MOB 35--

The Android app currently supports two methods for bridge provisioning. A secure method using the HAP SDK nodejs reference app, and a native unsecure method via HTTP.

Provisioning passes off Wifi Credentials to the bridge

- Network SSID
- Network Security
- Network Password
- Network Channel

Marvell Chip creates Wireless AP that we connect to
WAC -- Wireless Accessory Configuration (Apple Chip) used by iOS app

Admin Account --- Send Commands to Bridge that are not in API
Postman --- Send Client API Calls

-----------------------------------

### AES

In AES the length of the key should be 
128-bit(16 bytes)
192-bit(24 bytes) 
256-bit(32 bytes).

[Source](https://tech-lead.blogspot.com/2013/07/how-to-encrypt-and-decrypt-with-aes.html)


For AES, your block length is always going to be 128 bits/16 bytes regardless of the key length used

IV: An initialization vector, much like a salt, introduces randomness. However, unlike a salt, which is applied to the secret key, the initialization vector is applied to the data to be encrypted. For example, an initialization vector will prevent identical output when encrypting two identical strings with the same cipher. This variation eliminates the ability to determine the value of raw data based upon knowledge of patterns in the encrypted cipher text.

AES is a block cipher. It only specifies how to encrypt or decrypt a 128-bit block. If your data is shorter or longer than that, you need something more sophisticated: you need to have a mode of operation, often called chaining mode because it specifies how to process one block after the other. The choice of chaining mode is largely independent of the choice of block processing algorithm

Counter (CTR) mode turns a block cipher into a stream cipher.


**Add to Terms**: 
In cryptography, a nonce is an arbitrary number that may only be used once. It is similar in spirit to a nonce word, hence the name. It is often a random or pseudo-random number issued in an authentication protocol to ensure that old communications cannot be reused in replay attacks. They can also be useful as initialization vectors and in cryptographic hash function. [Wiki](https://en.wikipedia.org/wiki/Cryptographic_nonce)

**Add to Acronyms**:
PBE: Password Based Encryption


devicePubKey bytes= 64
devicePubKey = ByteString[size=32 md5=a7261b9d58b1a93ad255f1c30f073554]

iv bytes= 32
iv = ByteString[size=16 data=c7905624ee2d607596742a705a0e4935]

data bytes= 128
data = ByteString[size=64 md5=ac1a427f20fe45377aefbc8c23965245]

sharedSecretX curve25519 pub/priv key length = 32
sharedSecretY provisioningPin SHA512 byte [] length= 64