### Questions

#### General

- JIRA Active Sprint ---> **Gastdruck**??? (Sprint Name?)

#### Hardware

- More Sensors?
- Another bridge?
- Friends + Family for Staging?

#### Android


**08-23-16**
- Will bringing in all of the Espresso + Test Support Libs push us over the dex limit?
    - Include Gradle Plugin for Dex Count so we can keep track of where it's at 
- Lots of unused methods in the NotionApi class? 
- We need to have a discussion in the coming weeks based on: 

In a hypothetical world where I am entirely up to speed and I know every line in the code base, we need map out and come up with an action/game plan taking everyone's POV into account: 

- Garret's Priorities
- Joe's Priorities
- Ryan/Brett's Priorities

**09-07-16**

- Beta == Staging? 
- GitHub Notionite???

**09-08-16**

- Different players in the hardware game and their role?
    - Vergent?
    - UA?
    - Others from Product Meeting

**09-13-16**

- What's the actual process to go from Alpha ---> Staging ---> Prod? 
- Difference between Android SDK Platform-tools vs. Build Tools?
- Release Schedule?
