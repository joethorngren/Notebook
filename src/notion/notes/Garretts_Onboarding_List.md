### Welcome Joe

- QR-Lib Migration
- New Relic
- Hans' Bugs (Slack)
- Smake/Leak Text
- Notification Ack
- 6.0.0 Settings Issue
- Flurry (memberships)

- Dev/Build/Deploy Breakdown
- Code Tour / Android(?) Review 
- iOS Feature Gap Review
- Crash/ANR Review
- Design Files
- Postman/API Docs
- Publish App
- Prioritize Tasks