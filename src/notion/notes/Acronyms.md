### General Notion:

| Acronym     | Meaning                               |
|:------------|:--------------------------------------|
| NAPA        | Notion Android Phone App              |

### Technical:
 
| Acronym     | Meaning                                        | Source                                                                                                |
|:------------|:-----------------------------------------------|:------------------------------------------------------------------------------------------------------|
| Apple's HAP | Homekit Accessory Protocol                     |                                                                                                       |
| SoC         | System-on-Chip                                 |                                                                                                       |
| OWASP       | Open Web Application Security Project          |                                                                                                       |
| HWMD        | Hardware Meta-Data                             | Notion                                                                                                |
| TOTP        | Time-based One Time Password                   | 1Password                                                                                             |
| OATU        | Over the Air Update                            |                                                                                                       |
| AAR         | Android Archive Library                        | http://goo.gl/mTr6hx                                                                                  |
| OWASP       | Open Web Application Security Project          |                                                                                                       |
| LQI         | Link Quality Indicator                         | https://e2e.ti.com/support/wireless_connectivity/w/design_notes/calculation-and-usage-of-lqi-and-rssi |
| JWT         | JSON Web Token                                 | https://www.owasp.org/index.php/REST_Security_Cheat_Sheet#                                            |
| SSID        | Service Set Identifier                         |                                                                                                       |


Cryptography:

| Acronym     | Meaning                                        | Source                                                                                                |
|:------------|:-----------------------------------------------|:------------------------------------------------------------------------------------------------------|
| MitM        | Man in the Middle Attack                       |                                                                                                       |
| AES         | Advanced Encryption Standard                   |                                                                                                       |
| SHA         | Secure Hashing Algorithm                       |                                                                                                       |
| JCE         | Java Cryptography Extension                    | http://goo.gl/mrVd4v                                                                                  |
| JSSE        | Java Secure Sockets Extension                  | http://goo.gl/mrVd4v                                                                                  |
| JAAS        | Java Authentication and Authorization Service  | http://goo.gl/mrVd4v                                                                                  |
| JGSS        | Java General Security Service                  | http://goo.gl/mrVd4v                                                                                  |
| SHA         | Secure Hash Algorithm                          | https://goo.gl/uZejQs                                                                                 |
| NIST        | National Institute of Standards and Technology | https://goo.gl/uZejQs                                                                                 |
| FIPS        | U.S. Federal Information Processing Standard   | https://goo.gl/uZejQs                                                                                 |
| MAC         | Message Authentication Code                    | https://goo.gl/mhjbnI                                                                                 |
| HMAC        | Keyed-Hash Message Authentication Code         | https://goo.gl/mhjbnI                                                                                 |
| PRNG        | Pseudo-Random Number Generator                 | https://www.owasp.org/index.php/Using_the_Java_Cryptographic_Extensions                               |
| CBC         | Certificate-Based Cryptography                 | |
| CBC         | Certificate-Based Cryptography                 | |


####  ???
- SUP jiras
- Migrate Staging 
- LQI 

### Non-Technical

| Acronym     | Meaning                               |
|:------------|:--------------------------------------|

### Unsorted

TSA pre



